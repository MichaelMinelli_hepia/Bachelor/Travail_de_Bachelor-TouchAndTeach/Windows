﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Newtonsoft.Json;
using TouchAndTeachWindows.Comm;
using TouchAndTeachWindows.Draw;
using TouchAndTeachWindows.Draw.DrawObjects;
using TouchAndTeachWindows.Draw.UserDrawingUI;
using TouchAndTeachWindows.Menu;
using TouchAndTeachWindows.Menu.MenuPen;
using TouchAndTeachWindows.Utils;
using Page = TouchAndTeachWindows.Draw.Page;
using PointConverter = TouchAndTeachWindows.Utils.PointConverter;

namespace TouchAndTeachWindows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainMenu Menu;

        private readonly DrawController _drawController;
        private PenSize _penSize;
        private PenType _penType;
        private GlobalNotification _globalNotification;


        public MainWindow()
        {
            InitializeComponent();
            DrawArea drawArea = DrawArea.GetDrawArea();
            DrawModel drawModel = DrawModel.GetDrawModel();

            _drawController = DrawController.GetDrawController();
            _drawController.Init(this);

            Grid.Children.Add(drawArea);
            Grid.SetRow(drawArea, 1);
            Grid.SetColumn(drawArea, 0);
            Grid.SetColumnSpan(drawArea, 3);
            Panel.SetZIndex(drawArea, 1000);

            LoadItems();

            drawModel.NewPage();

            //start TCP and UDP servers
            Global.tcpServer = new TcpServer(Dispatcher);
            Global.udpServer = new UdpServer();

            _drawController.UserDrawingAdded += drawController_UserDrawingAdded;
            _drawController.SelectionAdded += _drawController_SelectionAdded;
            MessageManager messageManager = MessageManager.GetMessageManager();
            messageManager.MessageArrived += MessageManagerOnMessageArrived;
            messageManager.NoMoreMessage += MessageManagerOnNoMoreMessage;
            //drawModel.Deserialize("Slide 2:Stroke;10.0;-65536;485,216;483,211;480,208;468,204;437,210;395,225;359,243;334,260;321,273;318,283;324,297;350,320;390,346;429,373;459,396;476,424;482,448;473,475;447,502;413,520;379,526;346,525;311,516;288,511;270,508;259,508;251,511;_Stroke;10.0;-16777216;491,141;470,138;442,136;410,140;374,149;327,168;278,200;223,257;180,320;148,390;131,455;128,514;139,566;162,602;203,624;252,626;312,609;378,574;430,525;471,465;505,401;523,338;532,277;525,226;497,192;456,178;405,179;389,182;_/Slide 3:Stroke;10.0;-65536;485,216;483,211;480,208;468,204;437,210;395,225;359,243;334,260;321,273;318,283;324,297;350,320;390,346;429,373;459,396;476,424;482,448;473,475;447,502;413,520;379,526;346,525;311,516;288,511;270,508;259,508;251,511;_Stroke;10.0;-16777216;491,141;470,138;442,136;410,140;374,149;327,168;278,200;223,257;180,320;148,390;131,455;128,514;139,566;162,602;203,624;252,626;312,609;378,574;430,525;471,465;505,401;523,338;532,277;525,226;497,192;456,178;405,179;389,182;_");
            //drawArea.RewriteBitmap();
        }

        void _drawController_SelectionAdded(Draw.SelectionUI.Selection selection)
        {
            Grid.Children.Add(selection);
            Grid.SetRow(selection, 1);
            Grid.SetColumn(selection, 0);
            Grid.SetColumnSpan(selection, 3);
            Panel.SetZIndex(selection, 2001);
        }

        void drawController_UserDrawingAdded(UserDrawing userDrawing)
        {
            Grid.Children.Add(userDrawing);
            Grid.SetRow(userDrawing, 1);
            Grid.SetColumn(userDrawing, 0);
            Grid.SetColumnSpan(userDrawing, 3);
            Panel.SetZIndex(userDrawing, 2001);
        }

        public void AddUserDrawingMenu(UserDrawing userDrawingParent, Point position)
        {
            UserDrawingMenu userDrawingMenu = new UserDrawingMenu(userDrawingParent);
            userDrawingMenu.RenderTransform = new TranslateTransform(position.X-userDrawingMenu.Width/2, position.Y-userDrawingMenu.Height/2);
            Grid.Children.Add(userDrawingMenu);
            Grid.SetRow(userDrawingMenu, 0);
            Grid.SetRowSpan(userDrawingMenu, 3);
            Grid.SetColumn(userDrawingMenu, 0);
            Grid.SetColumnSpan(userDrawingMenu, 3);
            Panel.SetZIndex(userDrawingMenu, 2002);
        }

        void LoadItems()
        {
            #region Menu

            Menu = new MainMenu(_drawController);
            Grid.Children.Add(Menu);
            Grid.SetRow(Menu, 1);
            Grid.SetColumn(Menu, 0);
            Grid.SetColumnSpan(Menu, 3);
            Panel.SetZIndex(Menu, 2500);

            #endregion

            #region GlobalNotification

            _globalNotification = new GlobalNotification();
            Grid.Children.Add(_globalNotification);
            Grid.SetRow(_globalNotification, 1);
            Grid.SetColumn(_globalNotification, 0);
            Panel.SetZIndex(_globalNotification, 2501);
            Matrix matrix = Matrix.Identity;
            matrix.OffsetX = 0;
            matrix.OffsetY = 10;
            _globalNotification.RenderTransform = new MatrixTransform(matrix);

            #endregion
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            Global.tcpServer.Stop();
            Global.udpServer.Stop();
            base.OnClosing(e);
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Left)
                _drawController.PreviousPage();

            if (e.Key == Key.Right)
                _drawController.NextPage();

            if (e.Key == Key.U)
                _drawController.Undo();

            if (e.Key == Key.M)
            {
                Menu.Visibility = Menu.Visibility == Visibility.Collapsed ? 
                    Visibility.Visible : Visibility.Collapsed;
            }

            if (e.Key == Key.B)
            {
                BitmapImage bitmapImage = new BitmapImage(new Uri(BaseUriHelper.GetBaseUri(this),
                        @"/TouchAndTeachWindows;component/ImgRes/youhou.jpg"));
                DrawObject drawObject = new DrawImage(bitmapImage);
                _drawController.SetCurrentPageBackground(drawObject);
            }

            if (e.Key == Key.D)
            {
                BitmapImage bitmapImage = new BitmapImage(new Uri(BaseUriHelper.GetBaseUri(this),
                        @"/TouchAndTeachWindows;component/ImgRes/youhou.jpg"));
                DrawObject drawObject = new DrawImage(bitmapImage);
                bitmapImage = new BitmapImage(new Uri(BaseUriHelper.GetBaseUri(this),
                        @"/TouchAndTeachWindows;component/ImgRes/Postit/postit_pin.png"));
                DrawObject drawObject2 = new DrawImage(bitmapImage);
                //_drawController.AddDrawObjectToCurrentPage(drawObject2);
                Page p = new Page("pasf");
                p.AddInactiveObject(drawObject);
                p.AddInactiveObject(drawObject2);
                _drawController.AddUserDrawing(p);
            }

            if (e.Key == Key.S)
            {
                string toto = JsonConvert.SerializeObject(_drawController.GetCurrentPage(), Formatting.Indented, JsonUtils.SerializeConverter);
                Page p = JsonConvert.DeserializeObject<Page>(toto, JsonUtils.DeserializeConverters);
                _drawController.AddPage(p);
                _drawController.NextPage();
            }

            if (e.Key == Key.P)
            {
                //DocToWPF.PDFViewer pdfViewer = new DocToWPF.PDFViewer(System.IO.Directory.GetCurrentDirectory() + "\\ImgRes\\Flyer_iutp.pdf");
                //DrawPDF drawPdf = new DrawPDF(pdfViewer, 4);
                //_drawController.GetCurrentPage().AddInactiveObject(drawPdf);

                //new System.Security.Permissions.ReflectionPermission(System.Security.Permissions.ReflectionPermissionFlag.MemberAccess);
                ////System.Security.Permissions.ReflectionPermissionFlag
                //var bmpSource = DocToWPF.PDF.ToBitmapSourceList(System.IO.Directory.GetCurrentDirectory() + "\\ImgRes\\Flyer_iutp.pdf");
                //var drawImage = new DrawImage(bmpSource[0]);
                //_drawController.SetCurrentPageBackground(drawImage);
            }
        }

        private void Window_ManipulationStarting(object sender, ManipulationStartingEventArgs e)
        {
            e.ManipulationContainer = this;
            e.Handled = true;
        }

        private void Rectangle_TouchDown(object sender, TouchEventArgs e)
        {
            e.Handled = true;
        }

        private void Rectangle_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Matrix matrix = Matrix.Identity;
            matrix.OffsetX = e.GetPosition(this).X - Menu.Width / 2;
            matrix.OffsetY = ActualHeight / 2 - Menu.Height / 2;
            OpenMenu(matrix);
            e.Handled = true;
        }

        private void Rectangle_TouchUp(object sender, TouchEventArgs e)
        {
            Matrix matrix = Matrix.Identity;
            matrix.OffsetX = e.TouchDevice.GetTouchPoint(this).Position.X - Menu.Width / 2;
            matrix.OffsetY = ActualHeight / 2 - Menu.Height / 2;
            OpenMenu(matrix);
            e.Handled = true;
        }

        public void OpenMenu(Matrix matrix)
        {
            Menu.Visibility = Visibility.Visible;
            Menu.RenderTransform = new MatrixTransform(matrix);
        }

        public void OpenMenu(Matrix matrix, MainMenu.SubMenu subMenu)
        {
            Menu.Visibility = Visibility.Visible;
            Menu.RenderTransform = new MatrixTransform(matrix);
            Menu.ChooseSubMenu(subMenu);
        }

        private void MessageManagerOnMessageArrived(Device device, Message message)
        {
            if (!_globalNotification.IsOpen)
                _globalNotification.ShowNotification();
        }

        private void MessageManagerOnNoMoreMessage()
        {
            _globalNotification.HideNotification();
        }
    }
}
