﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace TouchAndTeachWindows.Converters
{
    public class IfTrueThenBrushElseWhiteConverter : IValueConverter
    {

        SolidColorBrush _backgroundColor = (SolidColorBrush)App.Current.Resources["SelectedAreaColorBrush"];

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is Boolean) || !(Boolean)value) return Brushes.White;
            if (!(parameter is string)) return _backgroundColor;

            try
            {
                return (SolidColorBrush)(new BrushConverter().ConvertFrom((string)parameter));
            }
            catch
            {
                return _backgroundColor;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
