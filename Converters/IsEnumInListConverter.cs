﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace TouchAndTeachWindows.Converters
{
    public class IsEnumInListConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is Enum)) return false;
            if (!(parameter is String)) return false;

            String[] enumList = ((string)parameter).Split('|');
            parameter = null;
            foreach (string enumParamString in enumList)
            {
                try
                {
                    dynamic enumParam = Enum.Parse(value.GetType(), enumParamString);
                    if (Enum.Equals(value, enumParam)) return true;
                }
                catch
                {
                    continue;
                }
            }

            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
