﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Data;

namespace TouchAndTeachWindows.Comm
{
    public class DevicesManager
    {
        public event Action<Device> NewDeviceConnected = device => { }; 
        public event Action<Device> DeviceDisconnected = device => { }; 

        private readonly ObservableCollection<Device> _devicesPermissions; //Use observableCollection so when Add/Remove, the binding is notified
        public ObservableCollection<Device> GetDevicesPermissions() { return _devicesPermissions; }

        public enum Mode { Restricted, FullAccess, Undefined }
        private Mode _currentMode = Mode.FullAccess;

        private DevicesManager()
        {
            _devicesPermissions = new ObservableCollection<Device>();
            BindingOperations.EnableCollectionSynchronization(_devicesPermissions, this); //make _devicePermissions thread safe
        }

        private static DevicesManager _devicesManager;
        public static DevicesManager GetDevicesManager()
        {
            return _devicesManager ?? (_devicesManager = new DevicesManager());
        }

        public void RegisterDevice(Device device)
        {
            Device exists = _devicesPermissions.FirstOrDefault(obj => obj.Socket == device.Socket);
            if (exists != null) return;
            device.Mode = _currentMode;
            _devicesPermissions.Add(device);
            NewDeviceConnected(device);
        }

        public void RemoveDevice(Device device)
        {
            Device elem = _devicesPermissions.FirstOrDefault(obj => obj.Socket == device.Socket);
            if (elem == null) return;

            _devicesPermissions.Remove(elem);
            DeviceDisconnected(elem);
        }

        public void SetGlobalMode(Mode mode)
        {
            _currentMode = mode;
            foreach (Device elem in _devicesPermissions)
            {
                elem.Mode = mode;
            }
        }

        public void SetMode(Device device, Mode mode)
        {
            Device elem = _devicesPermissions.FirstOrDefault(obj => obj.Socket == device.Socket);
            if (elem != null)
            {
                elem.Mode = mode;
            }
        }

        public Mode GetMode(Device device)
        {
            Device elem = _devicesPermissions.FirstOrDefault(obj => obj.Socket == device.Socket);
            return elem != null ? elem.Mode : Mode.Undefined;
        }
    }
}