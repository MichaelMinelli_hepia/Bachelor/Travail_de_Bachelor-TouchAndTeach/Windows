﻿using System.ComponentModel;

namespace TouchAndTeachWindows.Comm
{
    public class Device : INotifyPropertyChanged
    {
        private DevicesManager.Mode _mode;
        public DevicesManager.Mode Mode
        {
            get { return _mode; }
            set
            {
                _mode = value;
                OnPropertyChanged("Mode");
            }
        }
        private string _userName;
        public string UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
                OnPropertyChanged("UserName");
            }
        }

        public string Socket { get; set; }

        public Device(string socket)
        {
            Socket = socket;
            UserName = socket;
            Mode = DevicesManager.Mode.Restricted;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
