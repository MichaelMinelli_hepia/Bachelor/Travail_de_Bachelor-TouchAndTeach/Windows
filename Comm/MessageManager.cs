﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Newtonsoft.Json;
using TouchAndTeachWindows.Draw;
using TouchAndTeachWindows.Draw.DrawObjects;
using TouchAndTeachWindows.Utils;

namespace TouchAndTeachWindows.Comm
{
    public class MessageManager
    {
        public event Action<Device, Message> MessageArrived = (device, message) => { }; 
        public event Action NoMoreMessage = () => { }; 

        private readonly ObservableCollection<Message> _messageCollection;
        public ObservableCollection<Message> GetMessageCollection() { return _messageCollection; }

        private static MessageManager _messageManager;
        public static MessageManager GetMessageManager()
        {
            return _messageManager ?? (_messageManager = new MessageManager());
        }

        private MessageManager()
        {
            _messageCollection = new ObservableCollection<Message>();
            BindingOperations.EnableCollectionSynchronization(_messageCollection, this); //make _messageCollection thread safe
        }

        /// <summary>
        /// To call when a new Message from a Device is arrived containing a serialized Page
        /// It adds a new element in the messageColletion which is binded to the NotilifactionList
        /// </summary>
        /// <param name="sender">Device that sends the message</param>
        /// <param name="message">Json string with serialized Page (drawObjects)</param>
        public void AddMessage(Device sender, string message)
        {
            Page page = JsonConvert.DeserializeObject<Page>(message, JsonUtils.DeserializeConverters);
            if (page == null) return;
            if (page.GetAllDrawObjects().Count <= 0) return;
            page.ResetUUIDs();

            Message m = new Message(sender, page);
            _messageCollection.Add(m);
            MessageArrived(sender, m);
        }

        /// <summary>
        /// To call when a new Message from a Device is arrived containing an image
        /// It adds a new element in the messageColletion which is binded to the NotilifactionList
        /// </summary>
        /// <param name="sender">Device that sends the message</param>
        /// <param name="bitmapImage"></param>
        public void AddMessage(Device sender, BitmapImage bitmapImage)
        {
            DrawObject drawObject = new DrawImage(bitmapImage);
            Page page = new Page();
            page.AddInactiveObject(drawObject);

            Message m = new Message(sender, page);
            _messageCollection.Add(m);
            MessageArrived(sender, m);
        }

        public void RemoveMessage(Message message)
        {
            if (message == null) return;
            _messageCollection.Remove(message);
            if (_messageCollection.Count == 0)
            {
                NoMoreMessage();
            }
        }
    }
}
