﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Threading;

namespace TouchAndTeachWindows.Comm
{
    class DeviceComm
    {
        private enum ReceiveMode
        {
            Normal,
            Image
        }

        private ReceiveMode _currentReceiveMode = ReceiveMode.Normal;

        private readonly DevicesManager _devicesManager = DevicesManager.GetDevicesManager();
        private bool _communicationOpen = true;
        private Device _device;
        private TcpClient _tcpClient;
        private readonly Dispatcher _dispatcher;
        private StreamWriter _writer;
        private StreamReader _reader;

        // image stuff
        private int _fileSize = 0;
        private byte[] _imgBytes;
        private int _imgBytesPos = 0;

        public DeviceComm(Dispatcher dispatcher)
        {
            _dispatcher = dispatcher;
        }

        public void Send(CommunicationCommand command)
        {
            if (command==null) return;
            if (!_communicationOpen) return;
            if (_writer==null) return;
            try
            {
                _writer.WriteLine(command.GetCommand());
                _writer.Flush();
            }
            catch
            {
                _devicesManager.RemoveDevice(_device);
                _tcpClient.Close();
            }
        }

        public void SendBytes(byte[] bytes)
        {
            if (bytes == null) return;
            if (bytes.Length == 0) return;
            if (!_communicationOpen) return;
            if (_writer==null) return;
            try
            {
                NetworkStream stream = _tcpClient.GetStream();
                stream.Write(bytes, 0, bytes.Length);
                stream.Flush();
            }
            catch
            {
                _devicesManager.RemoveDevice(_device);
                _tcpClient.Close();
            }
        }

        public void DoComm(object client)
        {
            _tcpClient = (TcpClient)client;
            if (_tcpClient == null) return;

            NetworkStream clientStream = _tcpClient.GetStream();
            _reader = new StreamReader(clientStream, Encoding.UTF8);
            _writer = new StreamWriter(clientStream, Encoding.UTF8);

            //register the new device
            string clientSocket = ((IPEndPoint)_tcpClient.Client.RemoteEndPoint).Address + ":";
            clientSocket += ((IPEndPoint)_tcpClient.Client.RemoteEndPoint).Port;
            _device = new Device(clientSocket);
            _devicesManager.RegisterDevice(_device);

            while(_communicationOpen)
            {
                try
                {
                    //All data received are Strings, readed by the StreamReader
                    if (_currentReceiveMode == ReceiveMode.Normal)
                    {
                        string messageReceived = _reader.ReadLine();

                        if (messageReceived == null) continue;
                        if (messageReceived.Length <= 0) continue;

                        DevicesManager.Mode currentAccessMode = _devicesManager.GetMode(_device);
                        CommunicationCommand response = null;
                        switch (currentAccessMode)
                        {
                            case DevicesManager.Mode.FullAccess:
                                Func<Object> action = () =>
                                {
                                    response = CommunicationCommand.HandleMessageReceived(_device, messageReceived);

                                    //Receive JPEG mode
                                    if (response.GetCommand().Contains("JpegMode"))
                                    {
                                        _currentReceiveMode = ReceiveMode.Image;

                                        //get file size 
                                        String[] res = response.GetCommand().Split(':');
                                        _fileSize = Int32.Parse(res[2]);
                                        _imgBytes = new byte[_fileSize];
                                        _imgBytesPos = 0;
                                    }
                                    return null;
                                };
                                ExecuteThreadSafeAction(action);

                                if (response != null)
                                {
                                    Send(response);
                                }
                                break;
                            case DevicesManager.Mode.Restricted:
                                Send(CommunicationCommand.CreateCommandSendTextMessage("Permission denied"));
                                break;
                        }
                    }

                    //data received are Images in bytes arrays
                    //Images are sent using multiple transmissions
                    //Once the image transmission is over, the _currentReceiveMode 
                    //switches back to NormalMode
                    else if (_currentReceiveMode == ReceiveMode.Image)
                    {
                        DevicesManager.Mode currentAccessMode = _devicesManager.GetMode(_device);
                        CommunicationCommand response = null;
                        switch (currentAccessMode)
                        {
                            case DevicesManager.Mode.FullAccess:
                                //read image
                                byte[] bytes = new byte[_fileSize];
                                int dataReadLength = clientStream.Read(bytes, 0, _fileSize);


                                for (int i = 0; i < dataReadLength; i++)
                                {
                                    _imgBytes[_imgBytesPos++] = bytes[i];
                                }

                                //check if the transmission is done
                                if (_imgBytesPos >= _fileSize)
                                {
                                    Func<Object> action = () =>
                                    {
                                        MemoryStream strmImg = new MemoryStream(_imgBytes);
                                        BitmapImage bitmapImage = new BitmapImage();
                                        bitmapImage.BeginInit();
                                        bitmapImage.StreamSource = strmImg;
                                        bitmapImage.EndInit();
                                        MessageManager.GetMessageManager().AddMessage(_device, bitmapImage);
                                        _currentReceiveMode = ReceiveMode.Normal;
                                        response = CommunicationCommand.CreateCommandSendTextMessage("image successfully transmitted");
                                        return null;
                                    };
                                    ExecuteThreadSafeAction(action);
                                }

                                if (response != null)
                                {
                                    Send(response);
                                }
                                break;
                            case DevicesManager.Mode.Restricted:
                                Send(CommunicationCommand.CreateCommandSendTextMessage("Permission denied"));
                                break;
                        }
                    }
                }
                catch
                {
                    _devicesManager.RemoveDevice(_device);
                    _tcpClient.Close();
                    return;
                }
            }
            _devicesManager.RemoveDevice(_device);
            _tcpClient.Close();
        }

        public void CloseConnection()
        {
            Send(CommunicationCommand.CreateCommandSendConnectionStatus("Disconnected"));
            _devicesManager.RemoveDevice(_device);
            _communicationOpen = false;
            if (_tcpClient != null)
            {
                _tcpClient.Close();
            }
        }

        private Object ExecuteThreadSafeAction(Func<Object> action)
        {
            object returnVal = Thread.CurrentThread != _dispatcher.Thread ? _dispatcher.Invoke(action) : action();
            return returnVal;
        }

    }
}