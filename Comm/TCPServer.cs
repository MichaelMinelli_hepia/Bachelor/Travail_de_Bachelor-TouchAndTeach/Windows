﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Threading;

namespace TouchAndTeachWindows.Comm
{
    class TcpServer
    {
        private const int Port = 2121;
        private readonly TcpListener _tcpListener;
        private readonly List<DeviceComm> _allConnection;
        public List<DeviceComm> GetAllConnection() { return _allConnection; }
        private bool _listen = true;
        private Dispatcher _dispatcher;

        public TcpServer(Dispatcher dispatcher)
        {
            _tcpListener = new TcpListener(IPAddress.Any, Port);
            _allConnection = new List<DeviceComm>();
            _dispatcher = dispatcher;
            new Thread(ClientListener).Start();
        }

        public void Stop()
        {
            //close all open connection
            _allConnection.ForEach(obj => obj.CloseConnection());

            //close server
            _listen = false;
            _tcpListener.Server.Close();
        }

        private void ClientListener()
        {
            _tcpListener.Start();
            while (_listen)
            {
                try
                {
                    //wait for connection
                    TcpClient client = _tcpListener.AcceptTcpClient();
                    Trace.WriteLine("New connection");

                    //handle connection
                    DeviceComm deviceComm = new DeviceComm(_dispatcher);
                    _allConnection.Add(deviceComm);
                    new Thread(deviceComm.DoComm).Start(client);
                }
                catch { }
            }
        }
    }
}
