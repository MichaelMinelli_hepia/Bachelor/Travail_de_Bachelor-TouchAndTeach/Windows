﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TouchAndTeachWindows.Comm
{
    class UdpServer
    {
        private readonly byte[] _dataToSend;
        private bool _listen = true;
        private readonly UdpClient _udpClient;

        public UdpServer()
        {
            IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Any, 1919);
            _udpClient = new UdpClient(ipEndPoint);
            const string socket = "ServerPort:2121";
            _dataToSend = new byte[1024];
            _dataToSend = Encoding.ASCII.GetBytes(socket);
            new Thread(ClientListener).Start();
        }

        public void Stop()
        {
            _listen = false;
            _udpClient.Close();
        }

        private void ClientListener()
        {
            IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
            while (_listen)
            {
                try
                {
                    byte[] receivedBytes = _udpClient.Receive(ref sender);
                    if (Encoding.Default.GetString(receivedBytes).Equals("AskPort"))
                    {
                        Trace.WriteLine("New UDP connection:" + sender.Address);
                        _udpClient.Send(_dataToSend, _dataToSend.Length, sender);
                    }
                }
                catch { }
            }
        }
    }
}
