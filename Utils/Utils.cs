﻿using System;

namespace TouchAndTeachWindows.Utils
{
    class Utils
    {
        private const String CharactersList = "abcdefghijklmnopqrstuvwxyz0123456789";
        private static readonly Random Rng = new Random();
        private const int UuidLenght = 10;

        public static String GenerateUuid()
        {
            char[] text = new char[UuidLenght];
            for (int i = 0; i < UuidLenght; i++)
            {
                text[i] = CharactersList[Rng.Next(CharactersList.Length)];
            }
            return new String(text);
        }
    }
}
