﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TouchAndTeachWindows.Draw;

namespace TouchAndTeachWindows.Menu
{
    /// <summary>
    /// Interaction logic for BarMenu.xaml
    /// </summary>
    public partial class BarMenu : UserControl
    {
        private readonly DrawController _controller = DrawController.GetDrawController();

        public BarMenu()
        {
            InitializeComponent();

            
            if (_controller.ZoomMode)
            {
                ManipulationButton.Selected = true;
                PenButton.Selected = false;
            }
            else
            {
                ManipulationButton.Selected = false;
                PenButton.Selected = true;
            }
        }

        public static readonly DependencyProperty OrientationProperty = DependencyProperty.Register(
                         "BarMenuOrientation",
                         typeof(Orientation),
                         typeof(BarMenu),
                         new PropertyMetadata(Orientation.Horizontal));

        public Orientation Orientation
        {
            get { return (Orientation)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }

        private void PreviousPage_Pressed(object sender, RoutedEventArgs e)
        {
            _controller.PreviousPage();
        }

        private void PenButton_Pressed(object sender, RoutedEventArgs e)
        {
            _controller.ZoomMode = false;
        }

        private void UndoButton_Pressed(object sender, RoutedEventArgs e)
        {
            _controller.Undo();
        }

        private void NextPage_Pressed(object sender, RoutedEventArgs e)
        {
            _controller.NextPage();
        }

        private void ManipulationButton_Pressed(object sender, RoutedEventArgs e)
        {
            _controller.ZoomMode = true;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SetPenToolBoxIcon();
            Draw.DrawModel.GetDrawModel().PropertyChanged += DrawModel_PropertyChanged;
            _controller.PropertyChanged += _controller_PropertyChanged;
        }

        

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            Draw.DrawModel.GetDrawModel().PropertyChanged -= DrawModel_PropertyChanged;
            _controller.PropertyChanged -= _controller_PropertyChanged;
        }

        private void DrawModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "CurrentDrawObjectType") return;
            SetPenToolBoxIcon();

        }

        private void _controller_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "ZoomMode") return;

            if (_controller.ZoomMode)
            {
                ManipulationButton.Selected = true;
                PenButton.Selected = false;
            }
            else
            {
                ManipulationButton.Selected = false;
                PenButton.Selected = true;
            }
        }

        private void SetPenToolBoxIcon()
        {
            DrawModel.DrawObjectType drawType = _controller.GetDrawObjectType();
            switch (drawType)
            {
                case DrawModel.DrawObjectType.Stroke:
                    PenButton.SetResourceReference(MenuButton.IconTemplateProperty, "PenTemplate");
                    PenButton.DataContext = _controller.GetPainter();
                    break;
                case DrawModel.DrawObjectType.Line:
                    PenButton.SetResourceReference(MenuButton.IconTemplateProperty, "LineTemplate");
                    PenButton.DataContext = _controller.GetPainter();
                    break;
                case DrawModel.DrawObjectType.Rectangle:
                    PenButton.SetResourceReference(MenuButton.IconTemplateProperty, "RectangleTemplate");
                    PenButton.DataContext = _controller.GetPainter();
                    break;
                case DrawModel.DrawObjectType.Highlighter:
                    PenButton.SetResourceReference(MenuButton.IconTemplateProperty, "HighlighterTemplate");
                    PenButton.DataContext = _controller.GetHighlighterPainter();
                    break;
                case DrawModel.DrawObjectType.Selection:
                    PenButton.SetResourceReference(MenuButton.IconTemplateProperty, "SelectionTemplate");
                    PenButton.DataContext = null;
                    break;
                case DrawModel.DrawObjectType.Eraser:
                    PenButton.SetResourceReference(MenuButton.IconTemplateProperty, "EraserTemplate");
                    PenButton.DataContext = null;
                    break;
                default:
                    break;
            }
        }
    }
}
