﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using TouchAndTeachWindows.Draw;

namespace TouchAndTeachWindows.Menu.MenuPen
{
    /// <summary>
    /// Interaction logic for PenToolBox.xaml
    /// </summary>
    public partial class PenToolBox : UserControl
    {
        private readonly DrawController _controller;

        public PenToolBox()
        {
            _controller = DrawController.GetDrawController();
            _controller.NewPointDown += (int id, Point pos) => KeepNewColor(ColorPicker.SelectedColor);


            InitializeComponent();

            DrawModel.DrawObjectType drawType = _controller.GetDrawObjectType();
            if (drawType == DrawModel.DrawObjectType.Highlighter)
                CrayonPenButton.DataContext = _controller.GetHighlighterPainter();
            else
                CrayonPenButton.DataContext = _controller.GetPainter();
        }

        private void DrawModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "CurrentDrawObjectType")
            {
                DrawModel.DrawObjectType drawType = _controller.GetDrawObjectType();
                switch (drawType)
                {
                    case DrawModel.DrawObjectType.Stroke :
                        CrayonPenButton.SetResourceReference(MenuButton.IconTemplateProperty, "PenTemplate");
                        CrayonPenButton.DataContext = _controller.GetPainter();
                        break;
                    case DrawModel.DrawObjectType.Line:
                        CrayonPenButton.SetResourceReference(MenuButton.IconTemplateProperty, "LineTemplate");
                        CrayonPenButton.DataContext = _controller.GetPainter();
                        break;
                    case DrawModel.DrawObjectType.Rectangle:
                        CrayonPenButton.SetResourceReference(MenuButton.IconTemplateProperty, "RectangleTemplate");
                        CrayonPenButton.DataContext = _controller.GetPainter();
                        break;
                    case DrawModel.DrawObjectType.Highlighter:
                        CrayonPenButton.SetResourceReference(MenuButton.IconTemplateProperty, "HighlighterTemplate");
                        CrayonPenButton.DataContext = _controller.GetHighlighterPainter();
                        break;
                    default:
                        break;
                }
            }
        }

        private void ColorPicker_ColorChanged(object sender, RoutedEventArgs e)
        {
            _controller.ZoomMode = false;
            DrawModel.DrawObjectType drawType = _controller.GetDrawObjectType();

            if (drawType == DrawModel.DrawObjectType.Highlighter)
                _controller.GetHighlighterPainter().Color = ColorPicker.SelectedColor;
            else
                _controller.GetPainter().Color = ColorPicker.SelectedColor;
        }

        private void colorButton_Pressed(object sender, RoutedEventArgs e)
        {
            _controller.ZoomMode = false;
            Color mediaColor = ((ColorButton)sender).Color.Color;
            System.Drawing.Color drawingColor = System.Drawing.Color.FromArgb(mediaColor.A, mediaColor.R, mediaColor.G, mediaColor.B);
            double hue, saturation, value;
            ColorToHSV(drawingColor, out hue, out saturation, out value);

            ColorPicker.Hue = hue;
            ColorPicker.CursorSvPos = new Point(saturation, value);

            DrawModel.DrawObjectType drawType = _controller.GetDrawObjectType();

            if (drawType == DrawModel.DrawObjectType.Highlighter)
                _controller.GetHighlighterPainter().Color = ((ColorButton)sender).Color;
            else
                _controller.GetPainter().Color = ((ColorButton)sender).Color;
        }

        private void KeepNewColor(SolidColorBrush newColor)
        {
            int buttonNum = ColorButtonsGrid.Children.OfType<ColorButton>().ToList().FindIndex(colB => colB.Color.Color == newColor.Color);
            if (buttonNum == -1)
            {
                for (int i = ColorButtonsGrid.Children.Count - 1; i > 0; i--)
                    ((ColorButton)ColorButtonsGrid.Children[i]).Color = ((ColorButton)ColorButtonsGrid.Children[i - 1]).Color.Clone();

                ((ColorButton)ColorButtonsGrid.Children[0]).Color = newColor.Clone();
            }
            else if (((ColorButton)ColorButtonsGrid.Children[0]).Color.Color != newColor.Color)
            {
                for (int i = ColorButtonsGrid.Children.Count - 1; i > 0; i--)
                    if (i <= buttonNum)
                        ((ColorButton)ColorButtonsGrid.Children[i]).Color = ((ColorButton)ColorButtonsGrid.Children[i - 1]).Color.Clone();

                ((ColorButton)ColorButtonsGrid.Children[0]).Color = newColor.Clone();
            }
        }

        /// <summary>
        /// Change color to HSV
        /// Found here http://stackoverflow.com/questions/359612/how-to-change-rgb-color-to-hsv
        /// </summary>
        /// <param name="color"></param>
        /// <param name="hue"></param>
        /// <param name="saturation"></param>
        /// <param name="value"></param>
        private void ColorToHSV(System.Drawing.Color color, out double hue, out double saturation, out double value)
        {
            int max = Math.Max(color.R, Math.Max(color.G, color.B));
            int min = Math.Min(color.R, Math.Min(color.G, color.B));

            hue = color.GetHue();
            saturation = (max == 0) ? 0 : 1d - (1d * min / max);
            value = max / 255d;
        }

        private void PenSize_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _controller.ZoomMode = false;
            Point position = e.GetPosition(OverlayMenus);
            OpenPenSizeControl(position);
        }

        private void PenSize_OnTouchDown(object sender, TouchEventArgs e)
        {
            _controller.ZoomMode = false;
            Point position = e.GetTouchPoint(OverlayMenus).Position;
            OpenPenSizeControl(position);
        }

        private void OpenPenSizeControl(Point position)
        {
            if (OverlayMenus.Children.OfType<PenSize>().Any())
            {
                OverlayMenus.Children.Clear();
                return;
            }

            PenSize penSize = new PenSize();
            OverlayMenus.Children.Add(penSize);
            penSize.RenderTransform = new TranslateTransform(position.X - penSize.Width / 2, position.Y);
        }

        private void CrayonPenButton_OnTouchDown(object sender, TouchEventArgs e)
        {
            _controller.ZoomMode = false;
            Point position = e.GetTouchPoint(OverlayMenus).Position;
            OpenPenTypeControl(position, e);
        }

        private void CrayonPenButton_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _controller.ZoomMode = false;
            Point position = e.GetPosition(OverlayMenus);
            OpenPenTypeControl(position, e);
        }

        private void OpenPenTypeControl(Point position, InputEventArgs inputArg)
        {
            if (OverlayMenus.Children.OfType<PenType>().Any())
            {
                OverlayMenus.Children.Clear();
                return;
            }

            PenType penType = new PenType();
            OverlayMenus.Children.Add(penType);
            penType.RenderTransform = new TranslateTransform(position.X - penType.Width / 2, position.Y - penType.Height / 2);
        }


        private void SelectButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _controller.ZoomMode = false;
            _controller.SetDrawObjectStyle(DrawModel.DrawObjectType.Selection);
        }

        private void SelectButton_TouchDown(object sender, TouchEventArgs e)
        {
            _controller.ZoomMode = false;
            _controller.SetDrawObjectStyle(DrawModel.DrawObjectType.Selection);
        }

        private void UndoButton_Pressed(object sender, RoutedEventArgs e)
        {
            _controller.Undo();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            DrawModel.GetDrawModel().PropertyChanged += DrawModel_PropertyChanged;
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            DrawModel.GetDrawModel().PropertyChanged -= DrawModel_PropertyChanged;
        }


        private void Zoom_Pressed(object sender, RoutedEventArgs e)
        {
            _controller.ZoomMode = !_controller.ZoomMode;
        }

        private void Eraser_Pressed(object sender, RoutedEventArgs e)
        {
            _controller.ZoomMode = false;
            _controller.SetDrawObjectStyle(DrawModel.DrawObjectType.Eraser);
        }
    }
}
