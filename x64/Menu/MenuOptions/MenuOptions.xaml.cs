﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TouchAndTeachWindows.Menu.MenuOptions
{
    /// <summary>
    /// Logique d'interaction pour MenuOptions.xaml
    /// </summary>
    public partial class MenuOptions : UserControl
    {
        private MainWindow _mainWindow;
        public MenuOptions()
        {
            InitializeComponent();
            _mainWindow = (MainWindow)Application.Current.MainWindow;
        }

        private void SaveButton_Pressed(object sender, RoutedEventArgs e)
        {
            if (ImportExport.SavePages())
                MessageBox.Show("Save successful"); //TODO: change MessageBox to the text notificaiton system
            //else
                //MessageBox.Show("Error saving");
        }

        private void LoadButton_Pressed(object sender, RoutedEventArgs e)
        {
            if (!ImportExport.LoadPages())
            {
                //MessageBox.Show("Error loading pages");
            }
        }

        private void AboutButton_Pressed(object sender, RoutedEventArgs e)
        {
            _mainWindow.Menu.ChooseSubMenu(MainMenu.SubMenu.About);
        }

        private void QuitButton_Pressed(object sender, RoutedEventArgs e)
        {
            MessageBoxButton btnMessageBox = MessageBoxButton.YesNo;
            MessageBoxImage icnMessageBox = MessageBoxImage.Warning;
            MessageBoxResult result = MessageBox.Show("Exit application?", "TouchAndTeach", btnMessageBox, icnMessageBox);
            if (result == MessageBoxResult.Yes)
                _mainWindow.Close();
        }
    }
}
