﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using TouchAndTeachWindows.Comm;
using TouchAndTeachWindows.Draw;
using Page = TouchAndTeachWindows.Draw.Page;

namespace TouchAndTeachWindows.Menu
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class MainMenu : UserControl
    {
        public enum SubMenu
        {
            NoSubMenu,
            ThumbnailsList,
            PenToolBox,
            DevicesList,
            NotificationList,
            Options,
            About
        };

        private readonly DrawController _controller;
        private Point _position;
        private Boolean _manipulated;
        private Boolean _mouseMovingMenu;
        private Point _mousePosOnMenuHandler;

        public MainMenu(DrawController controller)
        {
            InitializeComponent();

            MessageManager messageManager = MessageManager.GetMessageManager();
            messageManager.MessageArrived += MessageArrived;
            messageManager.NoMoreMessage += NoMoreMessage;

            Visibility = Visibility.Collapsed;
            _controller = controller;
            _controller.ThumbnailAdded += ThumbnailAdded;

            DrawModel.DrawObjectType drawType = _controller.GetDrawObjectType();
            if (drawType == DrawModel.DrawObjectType.Highlighter)
                ShowPenToolBox.DataContext = _controller.GetHighlighterPainter();
            else
                ShowPenToolBox.DataContext = _controller.GetPainter();

            //controller.RefreshThumbnails();
            //controller.ThumbnailRemoving += ThumbnailRemoved;
        }


        private void SetPenToolBoxIcon()
        {
            DrawModel.DrawObjectType drawType = _controller.GetDrawObjectType();
            switch (drawType)
            {
                case DrawModel.DrawObjectType.Stroke:
                    ShowPenToolBox.SetResourceReference(MenuButton.IconTemplateProperty, "PenTemplate");
                    ShowPenToolBox.DataContext = _controller.GetPainter();
                    break;
                case DrawModel.DrawObjectType.Line:
                    ShowPenToolBox.SetResourceReference(MenuButton.IconTemplateProperty, "LineTemplate");
                    ShowPenToolBox.DataContext = _controller.GetPainter();
                    break;
                case DrawModel.DrawObjectType.Rectangle:
                    ShowPenToolBox.SetResourceReference(MenuButton.IconTemplateProperty, "RectangleTemplate");
                    ShowPenToolBox.DataContext = _controller.GetPainter();
                    break;
                case DrawModel.DrawObjectType.Highlighter:
                    ShowPenToolBox.SetResourceReference(MenuButton.IconTemplateProperty, "HighlighterTemplate");
                    ShowPenToolBox.DataContext = _controller.GetHighlighterPainter();
                    break;
                case DrawModel.DrawObjectType.Selection:
                    ShowPenToolBox.SetResourceReference(MenuButton.IconTemplateProperty, "SelectionTemplate");
                    ShowPenToolBox.DataContext = null;
                    break;
                case DrawModel.DrawObjectType.Eraser:
                    ShowPenToolBox.SetResourceReference(MenuButton.IconTemplateProperty, "EraserTemplate");
                    ShowPenToolBox.DataContext = null;
                    break;
                default:
                    break;
            }
        }


        private void DrawModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "CurrentDrawObjectType")
            {
                SetPenToolBoxIcon();
            }
        }

        private void Controller_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "ZoomMode")
            {
                if (_controller.ZoomMode)
                {
                    ShowPenToolBox.SetResourceReference(MenuButton.IconTemplateProperty, "ManipulationTemplate");
                    ShowPenToolBox.DataContext = null;
                }
                else
                    SetPenToolBoxIcon();
            }
        }

        public void ChooseSubMenu(SubMenu subMenu)
        {
            UnselectAll();
            switch (subMenu)
            {
                case SubMenu.NoSubMenu:
                    HideSubMenuItems();
                    return;
                case SubMenu.ThumbnailsList:
                    ShowPageList.Selected = true;
                    _controller.RefreshAllThumbnails();
                    HideSubMenuItems();
                    ThumbnailsList.Visibility = Visibility.Visible;
                    break;
                case SubMenu.PenToolBox:
                    ShowPenToolBox.Selected = true;
                    HideSubMenuItems();
                    PenToolBox.Visibility = Visibility.Visible;
                    break;
                case SubMenu.DevicesList:
                    ShowDevicesList.Selected = true;
                    HideSubMenuItems();
                    DevicesList.Visibility = Visibility.Visible;
                    break;
                case SubMenu.NotificationList:
                    NotificationButton.Selected = true;
                    HideSubMenuItems();
                    NotificationList.Visibility = Visibility.Visible;
                    break;
                case SubMenu.Options:
                    OptionsButton.Selected = true;
                    HideSubMenuItems();
                    MenuOptions.Visibility = Visibility.Visible;
                    break;
                case SubMenu.About:
                    OptionsButton.Selected = true;
                    HideSubMenuItems();
                    MenuAbout.Visibility = Visibility.Visible;
                    break;
            }
        }


        private void UserControl_ManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            if (_mouseMovingMenu || (!_manipulated && !(e.DeltaManipulation.Translation.LengthSquared > 50))) return;

            _manipulated = true;
            Matrix matrix = ((MatrixTransform)RenderTransform).Matrix;
            matrix.Translate(e.DeltaManipulation.Translation.X, e.DeltaManipulation.Translation.Y);
            RenderTransform = new MatrixTransform(matrix);
        }

        private void UserControl_ManipulationCompleted(object sender, ManipulationCompletedEventArgs e)
        {
            _manipulated = false;
            e.Handled = true;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            _position = TranslatePoint(new Point(0,0), (UIElement)Parent);
            DrawModel.GetDrawModel().PropertyChanged += DrawModel_PropertyChanged;
            _controller.PropertyChanged += Controller_PropertyChanged;

            //Binding zoomBinding = new Binding();
            //zoomBinding.Source = _controller;
            //zoomBinding.Path = new PropertyPath("ZoomMode");
            //zoomBinding.Mode = BindingMode.TwoWay;
            //BindingOperations.SetBinding(Zoom, MenuButton.SelectedProperty, zoomBinding);
        }

        

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            DrawModel.GetDrawModel().PropertyChanged -= DrawModel_PropertyChanged;
        }


        private void Rectangle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (_manipulated) return;

            _mouseMovingMenu = true;
            _mousePosOnMenuHandler = e.GetPosition(MenuHandler);
            Mouse.Capture(this);
        }

        private void UserControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            _mouseMovingMenu = false;
            Mouse.Capture(this, CaptureMode.None);
        }

        private void UserControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (!_mouseMovingMenu) return;

            Point newMenuPos = (Point)(e.GetPosition(MenuHandler) - _mousePosOnMenuHandler);
            Matrix matrix = ((MatrixTransform)RenderTransform).Matrix;
            matrix.Translate(newMenuPos.X, newMenuPos.Y);
            RenderTransform = new MatrixTransform(matrix);
        }


        private void ThumbnailAdded(Page page)
        {
            //ThumbnailsList.InsertElement(_controller.Thumbnails[page], ThumbnailsList.Count - 1);
            ThumbnailsList.AddElement(_controller.Thumbnails[page]);
            _controller.Thumbnails[page].Margin = new Thickness(5, 0, 5, 0);
            _controller.Thumbnails[page].Pressed += (sender, args) => _controller.ChoosePage(page);
            //StackPanelThumbnails.Children.Add(_controller.Thumbnails[page]);
        }

        //private void ThumbnailRemoved(Thumbnail thumbnail)
        //{
        //    stackPanelThumbnails.Children.Remove(thumbnail);
        //}

        private void HideSubMenuItems()
        {
            foreach (UIElement child in SubMenuGrid.Children)
            {
                child.Visibility = Visibility.Collapsed;
            }
        }

        private void UnselectAll()
        {
            foreach (MenuButton button in MainIcons.Children.OfType<MenuButton>())
                    button.Selected = false;
        }

        private void previousPage_Pressed(object sender, RoutedEventArgs e)
        {
            _controller.PreviousPage();
        }

        private void showPageList_Pressed(object sender, RoutedEventArgs e)
        {
            ChooseSubMenu(SubMenu.ThumbnailsList);
        }

        private void showPenToolBox_Pressed(object sender, RoutedEventArgs e)
        {
            ChooseSubMenu(SubMenu.PenToolBox);
        }

        private void undo_Pressed(object sender, RoutedEventArgs e)
        {
            _controller.Undo();
        }

        private void showDevicesList_Pressed(object sender, RoutedEventArgs e)
        {
            ChooseSubMenu(SubMenu.DevicesList);
        }

        private void nextPage_Pressed(object sender, RoutedEventArgs e)
        {
            _controller.NextPage();
        }


        private void NotificationButton_Pressed(object sender, RoutedEventArgs e)
        {
            ChooseSubMenu(SubMenu.NotificationList);
        }

        private void OptionsButton_Pressed(object sender, RoutedEventArgs e)
        {
            ChooseSubMenu(SubMenu.Options);
        }

   
        private void MessageArrived(Device device, Message message)
        {
            NotificationButton.SetResourceReference(MenuButton.IconTemplateProperty, "NotifRedTemplate");
        }

        private void NoMoreMessage()
        {
            NotificationButton.SetResourceReference(MenuButton.IconTemplateProperty, "NotifTemplate");
        }

        private void CloseButton_Pressed(object sender, RoutedEventArgs e)
        {
            Visibility = Visibility.Collapsed;
        }
    }
}
