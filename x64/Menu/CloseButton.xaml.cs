﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace TouchAndTeachWindows.Menu
{
    /// <summary>
    /// Interaction logic for CloseButton.xaml
    /// </summary>
    public partial class CloseButton : UserControl
    {
        #region Events Declaration
        public static readonly RoutedEvent PressedEvent = EventManager.RegisterRoutedEvent("CloseButtonPressed",
            RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(CloseButton));

        public event RoutedEventHandler Pressed
        {
            add { AddHandler(PressedEvent, value); }
            remove { RemoveHandler(PressedEvent, value); }
        }

        private readonly RoutedEventArgs _pressedArgs = new RoutedEventArgs(PressedEvent);
        #endregion

        //#region Public Attributes

        //public SolidColorBrush color
        //{
        //    get { return (SolidColorBrush)Resources["color"]; }
        //    set { Resources["color"] = value; }
        //}

        //public Boolean dissipate { get; set; }

        //#endregion


        public CloseButton()
        {
            InitializeComponent();
        }

        private void UserControl_TouchUp(object sender, TouchEventArgs e)
        {
            RaiseEvent(_pressedArgs);
            e.Handled = true;
        }

        private void UserControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            RaiseEvent(_pressedArgs);
            e.Handled = true;
        }

        private void UserControl_TouchDown(object sender, TouchEventArgs e)
        {
            e.Handled = true;
        }
    }
}
