﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

//using Interface_V3;

namespace TouchAndTeachWindows.Menu
{
    /// <summary>
    /// Color picker with a hue selector and saturation / level selector. Part of the code comes 
    /// from http://silverlightcontrib.codeplex.com/, but must of it has been simplified, improved
    /// and adapted for Interface_V3.
    /// </summary>
    public partial class ColorPicker : UserControl //, ITouchUserControl
    {

        #region Event declaration
        public static readonly RoutedEvent ColorChangedEvent = EventManager.RegisterRoutedEvent("ColorChanged",
            RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ColorPicker));

        public event RoutedEventHandler ColorChanged
        {
            add { AddHandler(ColorChangedEvent, value); }
            remove { RemoveHandler(ColorChangedEvent, value); }
        }

        readonly RoutedEventArgs _colorChangedArgs = new RoutedEventArgs(ColorChangedEvent);
        #endregion

        // Touching areas
        //TouchArea hueArea;
        //TouchArea sampleArea;

        private int _fingerInHue = int.MaxValue;
        private int _fingerInSv = int.MaxValue;

        private bool _mouseInSample = false;
        private bool _mouseInHue = false;

        private Point _cursorSvPos;
        /// <summary>
        /// Saturation and value quantity.
        /// Saturation is from 0 (cursor on left) to 1 (cursor on right)
        /// Value is from 0 (cursor on bottom) to 1 (cursor on top)
        /// </summary>
        public Point CursorSvPos
        {
            get { return _cursorSvPos; }
            set
            {
                // S and V have to be between 0 and 1
                _cursorSvPos = new Point(Math.Min(Math.Max(value.X, 0), 1), Math.Min(Math.Max(value.Y, 0), 1));
                // Change the cursor's position
                SampleSelector.RenderTransform = new TranslateTransform(_cursorSvPos.X * Sample.ActualWidth - 4, (1 - _cursorSvPos.Y) * Sample.ActualHeight - 4);
                // Change actual color with the new Saturation and Value
                SelectedColor.Color = GetFinalColor();
                // raise an event to warn the change of color
                RaiseEvent(_colorChangedArgs);
            }
        }

        double _hue;
        /// <summary>
        /// Hue value, from 0 (cursor on top) to 360 (cursor on bottom)
        /// </summary>
        public double Hue
        {
            get { return _hue; }
            set
            {
                //Hue has to be between 0 and 360
                _hue = Math.Max(Math.Min(value, 360), 0);
                // Change cursor position
                HueSelector.RenderTransform = new TranslateTransform(0, (_hue / 360) * HueArea.ActualHeight - 2);
                //hueSelector.Margin = new Thickness(0, (_hue / 360) * (mainGrid.RowDefinitions[0].Height.Value * mainGrid.Height), 0, 0);

                // Change color in the SV color selection
                Color c = GetColorFromHuePosition();
                ColorSample.Fill = new SolidColorBrush(c);
                // Change actual color with the new Hue
                SelectedColor.Color = GetFinalColor();

                // Raise an event to warn the change of color
                RaiseEvent(_colorChangedArgs);
            }
        }

        private float _screenHeight = (int)System.Windows.SystemParameters.PrimaryScreenHeight;
        private float _screenWidth = (int)System.Windows.SystemParameters.PrimaryScreenWidth;

        /// <summary>
        /// Init the user control
        /// </summary>
        public ColorPicker()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Activated when the control is loaded.
        /// Init the tactile areas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //Size hueSize = new Size((mainGrid.ColumnDefinitions[0].Width.Value * ActualWidth) / screenWidth,
            //    (mainGrid.RowDefinitions[0].Height.Value * ActualHeight) / screenHeight);
            //Size sampleSize = new Size((mainGrid.ColumnDefinitions[1].Width.Value * ActualWidth) / screenWidth,
            //    hueSize.Height);

            //Point pos = PointToScreen(new Point(0, 0));
            //pos = new Point(pos.X / screenWidth, pos.Y / screenHeight);

            //hueArea = new TouchArea((float)pos.X, (float)pos.Y, (float)(pos.X + hueSize.Width), (float)(pos.Y + hueSize.Height), 0, true);
            //sampleArea = new TouchArea((float)(pos.X + hueSize.Width), (float)pos.Y, (float)(pos.X + hueSize.Width + sampleSize.Width), (float)(pos.Y + hueSize.Height), 0, true);

            //Interface_hepia.AddArea(hueArea);
            //Interface_hepia.AddArea(sampleArea);
        }



        /// <summary>
        /// Get the color corresponding to the hue position
        /// </summary>
        /// <returns></returns>
        private Color GetColorFromHuePosition()
        {
            double huePosTemp = (Hue / 360) * (HueLinGradient.GradientStops.Count - 1);
            byte mod = (byte)((huePosTemp * 255) % 255);
            byte diff = (byte)(255 - mod);

            switch ((int)huePosTemp)
            {
                case 0: return Color.FromArgb(255, 255, mod, 0);
                case 1: return Color.FromArgb(255, diff, 255, 0);
                case 2: return Color.FromArgb(255, 0, 255, mod);
                case 3: return Color.FromArgb(255, 0, diff, 255);
                case 4: return Color.FromArgb(255, mod, 0, 255);
                case 5: return Color.FromArgb(255, 255, 0, diff);
                case 6: return Color.FromArgb(255, 255, mod, 0);
                default: return Colors.Black;
            }
        }

        /// <summary>
        /// Get color with actual hue, saturation and value
        /// </summary>
        /// <returns>Final color</returns>
        private Color GetFinalColor()
        {
            double v = _cursorSvPos.Y;
            double s = _cursorSvPos.X;
            //double h = huePos * 360;

            return HSVToRGB(Hue, s, v);
        }


        /// <summary>
        /// Copy of the code found here
        /// http://stackoverflow.com/questions/97646/how-do-i-determine-darker-or-lighter-color-variant-of-a-given-color
        /// </summary>
        /// <param name="h">Hue</param>
        /// <param name="s">Saturation</param>
        /// <param name="v">Value</param>
        private Color HSVToRGB(double h, double s, double v)
        {
            int i;
            double f, p, q, t;
            double r, g, b;

            if (s == 0)
            {
                // achromatic (grey)
                r = g = b = v;
            }
            else
            {
                h /= 60;                        // sector 0 to 5
                i = (int)Math.Floor(h);
                f = h - i;                      // factorial part of h
                p = v * (1 - s);
                q = v * (1 - s * f);
                t = v * (1 - s * (1 - f));
                switch (i)
                {
                    case 0:
                        r = v;
                        g = t;
                        b = p;
                        break;
                    case 1:
                        r = q;
                        g = v;
                        b = p;
                        break;
                    case 2:
                        r = p;
                        g = v;
                        b = t;
                        break;
                    case 3:
                        r = p;
                        g = q;
                        b = v;
                        break;
                    case 4:
                        r = t;
                        g = p;
                        b = v;
                        break;
                    default:                // case 5:
                        r = v;
                        g = p;
                        b = q;
                        break;
                }
            }
            r *= 255;
            g *= 255;
            b *= 255;
            return Color.FromArgb(255, (byte)r, (byte)g, (byte)b);
        }

        private void Grid_TouchDown(object sender, TouchEventArgs e)
        {
            Hue = e.GetTouchPoint(HueArea).Position.Y / HueArea.ActualHeight * 360;
            e.Handled = true;
        }

        private void Grid_TouchMove(object sender, TouchEventArgs e)
        {
            Hue = e.GetTouchPoint(HueArea).Position.Y / HueArea.ActualHeight * 360;
            e.Handled = true;
            //ReleaseTouchCapture(e.TouchDevice);
        }

        private void sample_TouchDown(object sender, TouchEventArgs e)
        {
            Point touch = e.GetTouchPoint((Grid)sender).Position;
            CursorSvPos = new Point(touch.X / ((Grid)sender).ActualWidth,
                1 - touch.Y / ((Grid)sender).ActualHeight);
            e.Handled = true;
        }

        private void sample_TouchMove(object sender, TouchEventArgs e)
        {
            Point touch = e.GetTouchPoint((Grid)sender).Position;
            CursorSvPos = new Point(touch.X / ((Grid)sender).ActualWidth,
                1 - touch.Y / ((Grid)sender).ActualHeight);
            //ReleaseTouchCapture(e.TouchDevice);
            e.Handled = true;

        }

        private void sample_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _mouseInSample = true;
            Point pos = e.GetPosition(Sample);
            CursorSvPos = new Point(pos.X / ((Grid)sender).ActualWidth,
                1 - pos.Y / ((Grid)sender).ActualHeight);
            e.Handled = true;
        }

        private void sample_MouseMove(object sender, MouseEventArgs e)
        {
            if (_mouseInSample)
            {
                Point pos = e.GetPosition(Sample);
                CursorSvPos = new Point(pos.X / ((Grid)sender).ActualWidth,
                    1 - pos.Y / ((Grid)sender).ActualHeight);
            }
            e.Handled = true;
        }

        private void sample_MouseLeave(object sender, MouseEventArgs e)
        {
            _mouseInSample = false;
            e.Handled = true;
        }

        private void sample_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            _mouseInSample = false;
            e.Handled = true;
        }

        private void hueArea_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _mouseInHue = true;
            Point pos = e.GetPosition(HueArea);
            Hue = pos.Y / HueArea.ActualHeight * 360;
            e.Handled = true;
        }

        private void hueArea_MouseMove(object sender, MouseEventArgs e)
        {
            if (_mouseInHue)
            {
                Point pos = e.GetPosition(HueArea);
                Hue = pos.Y / HueArea.ActualHeight * 360;
            }
            e.Handled = true;
        }

        private void hueArea_MouseLeave(object sender, MouseEventArgs e)
        {
            _mouseInHue = false;
            e.Handled = true;
        }

        private void hueArea_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            _mouseInHue = false;
            e.Handled = true;
        }

        ///// <summary>
        ///// Each time a finger is moving in the hue area, the Hue is changed
        ///// </summary>
        ///// <param name="finger"></param>
        //private void HueAreaMoving(Finger finger)
        //{
        //    if (finger.id == fingerInHue)
        //        Dispatcher.Invoke(new Action(delegate()
        //        {
        //            Hue = (finger.Y - hueArea.YleftTop) / (hueArea.YrightBottom - hueArea.YleftTop) * 360;
        //        }));
        //}

        ///// <summary>
        ///// Associate new finger in hue with hue control
        ///// </summary>
        ///// <param name="finger"></param>
        //private void HueAreaTouchingDown(Finger finger)
        //{
        //    fingerInHue = finger.id;
        //}

        ///// <summary>
        ///// Each time a finger is moving in the SV Area the Saturation and Value are changed
        ///// </summary>
        ///// <param name="finger"></param>
        //private void SVAreaMoving(Finger finger)
        //{
        //    if (finger.id == fingerInSV)
        //        sampleSelector.Dispatcher.Invoke(new Action(delegate()
        //        {
        //            CursorSVPos = new Point((finger.X - sampleArea.XleftTop) / (sampleArea.XrightBottom - sampleArea.XleftTop),
        //                1 - (finger.Y - sampleArea.YleftTop) / (sampleArea.YrightBottom - sampleArea.YleftTop));
        //        }));
        //}

        ///// <summary>
        ///// Associate new finger in SV area with SV control
        ///// </summary>
        ///// <param name="finger"></param>
        //private void SVAreaTouchingDown(Finger finger)
        //{
        //    fingerInSV = finger.id;
        //}
    }
}
