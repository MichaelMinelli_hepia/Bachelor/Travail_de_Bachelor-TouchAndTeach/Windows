﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TouchAndTeachWindows.Draw;

namespace TouchAndTeachWindows.Menu
{
    public partial class BarMenuMini : UserControl
    {
        private readonly DrawController _controller = DrawController.GetDrawController();

        public BarMenuMini()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty OrientationProperty = DependencyProperty.Register(
                         "BarMenuMiniOrientation",
                         typeof(Orientation),
                         typeof(BarMenu),
                         new PropertyMetadata(Orientation.Horizontal));

        public Orientation Orientation
        {
            get { return (Orientation)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }

        private void PreviousPage_Pressed(object sender, RoutedEventArgs e)
        {
            _controller.PreviousPage();
        }

        private void NextPage_Pressed(object sender, RoutedEventArgs e)
        {
            _controller.NextPage();
        }
    }
}
