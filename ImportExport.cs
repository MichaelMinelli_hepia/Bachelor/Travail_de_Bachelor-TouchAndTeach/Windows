﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using DocToWPF;
using Microsoft.Win32;
using Newtonsoft.Json;
using TouchAndTeachWindows.Draw;
using TouchAndTeachWindows.Draw.DrawObjects;
using TouchAndTeachWindows.Utils;
using System.Globalization;

namespace TouchAndTeachWindows
{
    public class ImportExport
    {

        private static string _openedFile;

        //public static string InkscapePath()
        //{
        //    string regKey1 = @"HKEY_CLASSES_ROOT\inkscape.svg\DefaultIcon";
        //    string regKey2 = @"HKEY_LOCAL_MACHINE\SOFTWARE\Classes\inkscape.svg\DefaultIcon";
        //    string regKey3 = @"HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\inkscape.exe";

        //    string path = (string)Microsoft.Win32.Registry.GetValue(regKey1, "", null);
        //    if (!string.IsNullOrWhiteSpace(path) && File.Exists(path = path.Trim('\"')))
        //        return path;

        //    path = (string)Microsoft.Win32.Registry.GetValue(regKey2, "", null);
        //    if (!string.IsNullOrWhiteSpace(path) && File.Exists(path = path.Trim('\"')))
        //        return path;

        //    path = (string)Microsoft.Win32.Registry.GetValue(regKey3, "", null);
        //    if (!string.IsNullOrWhiteSpace(path) && File.Exists(path = path.Trim('\"')))
        //        return path;

        //    return null;
        //}


        public static bool SaveAllPagesToTAT(string path)
        {
            String serializedPages = DrawController.GetDrawController().Serialize();

            if (serializedPages == null)
            {
                return false;
            }

            using (FileStream zipToOpen = new FileStream(path, FileMode.Create))
            {
                using (ZipArchive archive = new ZipArchive(zipToOpen, ZipArchiveMode.Update))
                {
                    ZipArchiveEntry drawObjectsEntry = archive.CreateEntry("drawobjects.json");
                    using (StreamWriter writer = new StreamWriter(drawObjectsEntry.Open()))
                    {
                        writer.Write(serializedPages);
                    }
                    if (!String.IsNullOrWhiteSpace(_openedFile))
                    {
                        try
                        {
                            archive.CreateEntryFromFile(_openedFile, Path.GetFileName(_openedFile));
                        }
                        catch (IOException e)
                        {
                            MessageBox.Show("Can't save if the document is open in an other application");
                            return false;
                        }
                    }
                }
            }

            return true;
        }


        private static bool SaveCurrentPageToSVG(string path)
        {
            string svgPage = DrawController.GetDrawController().GetSVG();

            using (StreamWriter writer = new StreamWriter(path))
                writer.Write(svgPage);

            return true;
        }

        private static void SVGPageToPDF(string svgPath, string pdfPath)
        {
            if (File.Exists(pdfPath))
                File.Delete(pdfPath);

            Process p = new Process();


            p.StartInfo.UseShellExecute = false;
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.FileName = @"./Dependencies/rsvg-convert.exe";
            p.StartInfo.Arguments = string.Format(CultureInfo.InvariantCulture, "-f pdf -o \"{0}\" \"{1}\"", pdfPath, svgPath);
            //p.StartInfo.Arguments = string.Format(CultureInfo.InvariantCulture, "-svg -f {0} -l {0} \"{1}\" \"{2}\"", Page + 1, _pdfViewer.FileName, tempFilePath);

            p.Start();

            //string output = p.StandardOutput.ReadToEnd();
            p.WaitForExit();
        }

        private static bool SaveCurrentPageToPDF(string path)
        {
            string svgPage = DrawController.GetDrawController().GetSVG();

            string tempSVGFilePath = System.IO.Path.GetTempPath() + System.IO.Path.GetRandomFileName() + ".svg";

            using (StreamWriter writer = new StreamWriter(tempSVGFilePath))
                writer.Write(svgPage);

            try
            {
                SVGPageToPDF(tempSVGFilePath, path);
            }
            catch (Exception e)
            {
                File.Delete(tempSVGFilePath);
                return false;
            }

            File.Delete(tempSVGFilePath);
            return true;
        }

        private static bool SaveAllPagesToPDF(string path)
        {
            List<string> allSVG = DrawController.GetDrawController().GetAllSVG();
            List<string> pdfPaths = new List<string>();
            foreach (string svg in allSVG)
            {
                string tempSVG = Path.ChangeExtension(Path.GetTempPath() + Path.GetRandomFileName(), "svg");
                string tempPDF = Path.ChangeExtension(Path.GetTempPath() + Path.GetRandomFileName(), "pdf");

                using (StreamWriter writer = new StreamWriter(tempSVG))
                    writer.Write(svg);

                try
                {
                    SVGPageToPDF(tempSVG, tempPDF);
                }
                catch (Exception e)
                {
                    File.Delete(tempSVG);
                    continue;
                }

                File.Delete(tempSVG);
                pdfPaths.Add(tempPDF);
            }


            try
            {
                Process p = new Process();

                p.StartInfo.UseShellExecute = false;
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.FileName = @"./Dependencies/poppler/exec/pdfunite.exe";
                p.StartInfo.Arguments = pdfPaths.Aggregate(new StringBuilder(), (result, pdf) => result.AppendFormat(CultureInfo.InvariantCulture, "\"{0}\"", pdf).Append(" ")).ToString() + string.Format(CultureInfo.InvariantCulture, "\"{0}\"", path);

                p.Start();

                p.WaitForExit();
            }
            catch (Exception e)
            {
                pdfPaths.ForEach(pdf => File.Delete(pdf));
                return false;
            }

            pdfPaths.ForEach(pdf => File.Delete(pdf));
            return File.Exists(path);
        }


        public static bool SavePages()
        {


            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                FileName = "Filename",
                DefaultExt = ".tat",
                Filter = "Touch And Teach file (*.tat)|*.tat" + // index = 1
                "|All pages to a PDF file (*.pdf)|*.pdf" + // index = 2
                "|Current page to a PDF file (*.pdf)|*.pdf" + // index 3
                "|Current page to a SVG file (*.svg)|*.svg", // index = 4
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)
            };

            bool? result = saveFileDialog.ShowDialog();
        
            if (result == true)
            {

                if (File.Exists(saveFileDialog.FileName))
                    try
                    {
                        File.Delete(saveFileDialog.FileName);
                    }
                    catch (System.IO.IOException exception)
                    {
                        MessageBox.Show(exception.Message);
                        return false;
                    }

                switch (saveFileDialog.FilterIndex)
                {
                    case 1:
                        return SaveAllPagesToTAT(saveFileDialog.FileName);
                    case 2:
                        return SaveAllPagesToPDF(saveFileDialog.FileName);
                    case 3:
                        return SaveCurrentPageToPDF(saveFileDialog.FileName);
                    case 4:
                        return SaveCurrentPageToSVG(saveFileDialog.FileName);
                    default:
                        break;
                }
            }
            return false;
        }



        public static bool LoadPages()
        {
            bool word = DocToWPF.Office.IsOfficeSoftwareAvailable(Office.OfficeComponent.Word);
            bool ppt = DocToWPF.Office.IsOfficeSoftwareAvailable(Office.OfficeComponent.PowerPoint);

            OpenFileDialog openFileDialog = new OpenFileDialog
            {
               Filter = "Document files (*.tat, *.pdf" + 
                        (word ? ", *.doc, *.docx" : "") +
                        (ppt ? ", *.ppt, *.pptx" : "") +
                        ")|*.tat;*.pdf" + 
                        (word ? ";*.doc;*.docx" : "") +
                        (ppt ? "*;*.ppt;*.pptx" : ""),
                        
               InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
               Title = "Open a previous project or import a file",
               Multiselect = false
            };

            if (openFileDialog.ShowDialog() == false) return false;

            string file = openFileDialog.FileName;

            if (String.IsNullOrWhiteSpace(file)) return false;

            DrawController.GetDrawController().RemoveAllPages();

            string extension = Path.GetExtension(file).ToLowerInvariant();
            PDFViewer pdfViewer = null;

            if (extension == ".tat")
            {
                string serializedPages = null;
                using (ZipArchive archive = ZipFile.OpenRead(file))
                {
                    if (archive.Entries.Count > 2)
                    {
                        MessageBox.Show("Unsupported .tat format");
                        return false;
                    }
                    foreach (ZipArchiveEntry entry in archive.Entries)
                    {
                        if (entry.FullName.Equals("drawobjects.json"))
                        {
                            using (StreamReader reader = new StreamReader(entry.Open()))
                            {
                                serializedPages = reader.ReadToEnd();
                            }
                        }
                        string localExtension = Path.GetExtension(entry.FullName);
                        if (localExtension != null)
                        {
                            switch (localExtension.ToLowerInvariant())
                            {
                                case ".pdf":
                                    //TODO: delete the temp file when program close, or when loading a new .tat
                                    _openedFile = Path.GetTempPath() + entry.FullName;
                                    if (File.Exists(_openedFile))
                                    {
                                        File.Delete(_openedFile);
                                    }
                                    entry.ExtractToFile(_openedFile);
                                    pdfViewer = new PDFViewer(_openedFile);
                                    break;
                                case ".doc":
                                case ".docx":
                                    _openedFile = Path.GetTempPath() + entry.FullName;
                                    if (File.Exists(_openedFile))
                                    {
                                        File.Delete(_openedFile);
                                    }
                                    entry.ExtractToFile(_openedFile);
                                    pdfViewer = DocToWPF.Word.ToPdfViewer(_openedFile);
                                    break;
                                case ".ppt":
                                case ".pptx":
                                    _openedFile = Path.GetTempPath() + entry.FullName;
                                    if (File.Exists(_openedFile))
                                    {
                                        File.Delete(_openedFile);
                                    }
                                    entry.ExtractToFile(_openedFile);
                                    pdfViewer = DocToWPF.PowerPoint.ToPdfViewer(_openedFile);
                                    break;
                            }
                        }
                    }
                }
                if (serializedPages != null)
                {
                    return DrawController.GetDrawController().Deserialize(serializedPages, pdfViewer);
                }
            }

            if (new[] {".pdf", ".doc", ".docx", ".ppt", ".pptx"}.Contains(extension))
                return OpenDocument(file);

            return false;
        }

        

        private static bool OpenDocument(string file)
        {
            if (String.IsNullOrWhiteSpace(file)) return false;
            _openedFile = file;

            string extension = Path.GetExtension(file).ToLowerInvariant();
            string fileName = Path.GetFileNameWithoutExtension(file);
            DrawController drawController = DrawController.GetDrawController();
            PDFViewer pdfViewer = null;

            switch (extension)
            {
                case ".pdf": pdfViewer = new PDFViewer(file);
                    break;
                case ".doc":
                case ".docx": pdfViewer = DocToWPF.Word.ToPdfViewer(file);
                    break;
                case ".ppt":
                case ".pptx": pdfViewer = DocToWPF.PowerPoint.ToPdfViewer(file);
                    break;
                default:
                    return false;
            }


            if (pdfViewer == null) return false;

            App.ToDisposeOnExit.Add(pdfViewer);

            

            for (int i = 0; i < pdfViewer.NumberOfPages; i++)
            {
                Page page = drawController.NewPage("page " + i);
                DrawPDF drawPdf = new DrawPDF(pdfViewer, i);
                drawController.SetBackgroundToPage(page, drawPdf);
            }
            drawController.ChoosePage(0);
            return true;
        }

    }
}
