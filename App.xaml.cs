﻿using System.Windows;
using System.Collections.Generic;
using System;

namespace TouchAndTeachWindows
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        public static List<IDisposable> ToDisposeOnExit = new List<IDisposable>();

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);

            DisposeAll();
        }

        public static void DisposeAll()
        {
            foreach (IDisposable toDispose in ToDisposeOnExit)
                toDispose.Dispose();

            ToDisposeOnExit.Clear();
        }
    }
}
