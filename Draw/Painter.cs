﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Media;
using Newtonsoft.Json;

namespace TouchAndTeachWindows.Draw
{
    public enum StrokeWidthEnum { VeryThin = 1, Thin = 5, Normal = 9, Thick = 15, VeryThick = 25, UltraThick = 40 };

    [JsonObject(MemberSerialization.OptIn)]
    public class Painter : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = (obj, arg) => { };

        private SolidColorBrush _color= Brushes.Black;
        private int _strokeWidth = (int)StrokeWidthEnum.Normal;

        [JsonProperty("Color")]
        public SolidColorBrush Color
        {
            get { return _color; }
            set
            {
                _color= value.Clone();
                PropertyChanged(this, new PropertyChangedEventArgs("Color"));
            }
        }

        [JsonProperty("StrokeWidth")]
        public int StrokeWidth
        {
            get { return _strokeWidth; }
            set
            {
                _strokeWidth = Math.Min((int)StrokeWidthEnum.UltraThick, Math.Max((int)StrokeWidthEnum.VeryThin, value));
                PropertyChanged(this, new PropertyChangedEventArgs("StrokeWidth"));
            }
        }

        public Painter() { }

        public Painter(Painter clone) : this()
        {
            _strokeWidth = clone._strokeWidth;
            _color = clone._color.Clone();
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + Color.Color.A.GetHashCode();
            hash = hash * 23 + Color.Color.R.GetHashCode();
            hash = hash * 23 + Color.Color.G.GetHashCode();
            hash = hash * 23 + Color.Color.B.GetHashCode();
            hash = hash * 23 + _strokeWidth.GetHashCode();
            return hash;
        }

        public override bool Equals(object obj)
        {
            return GetHashCode() == obj.GetHashCode();
        }

        public static bool operator ==(Painter painter1, Painter painter2)
        {
            return painter1.GetHashCode() == painter2.GetHashCode();
        }

        public static bool operator !=(Painter painter1, Painter painter2)
        {
            return painter1.GetHashCode() != painter2.GetHashCode();
        }
    }
}
