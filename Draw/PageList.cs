﻿using System.Collections.Generic;

namespace TouchAndTeachWindows.Draw
{
    public class PageList : List<Page>
    {
        private int _currentElement = 0;

        public Page Get(int pageNbr)
        {
            if (pageNbr < 0 || pageNbr >= Count) return null;
            _currentElement = pageNbr;
            return this[pageNbr];
        }

        public bool SetCurrent(Page page)
        {
            int index = -1;
            if (page != null)
                index = IndexOf(page);
            if (index == -1)
                return false;

            _currentElement = index;
            return true;
        }

        public Page GetNext()
        {
            return _currentElement < Count - 1 ? this[++_currentElement] : null;
        }

        public Page GetPrevious()
        {
            return _currentElement > 0 ? this[--_currentElement] : null;
        }

        public Page GetCurrentPage()
        {
            return _currentElement < Count ? this[_currentElement] : null;
        }
    }
}
