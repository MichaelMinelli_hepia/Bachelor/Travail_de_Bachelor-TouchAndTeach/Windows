﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using Newtonsoft.Json;
using TouchAndTeachWindows.Draw.DrawObjects;
using TouchAndTeachWindows.Utils;
using System.Diagnostics;
using DocToWPF;
using System.ComponentModel;
using System.Xml.Linq;

namespace TouchAndTeachWindows.Draw
{
    public class DrawModel : INotifyPropertyChanged
    {
        public enum DrawObjectType { Stroke, Rectangle, Ellipse, Eraser, Selection, Line, Highlighter }

        public event PropertyChangedEventHandler PropertyChanged = (obj, arg) => { };

        private Matrix _pageTransform;
        private readonly DrawController _drawController;
        private static DrawModel _drawModel;

        private string _highlightUuid = null;

        private DrawObjectType _currentDrawObjectType = DrawObjectType.Stroke;
        public DrawObjectType CurrentDrawObjectType
        {
            get { return _currentDrawObjectType; }
            set
            {
                _currentDrawObjectType = value;
                PropertyChanged(this, new PropertyChangedEventArgs("CurrentDrawObjectType"));
            }
        }


        public PageList PageList
        {
            get;
            private set;
        }

        private readonly Painter _currentPainter;
        private readonly Painter _highlighterPainter;
        

        private DrawModel()
        {
            PageList = new PageList();
            _currentPainter = new Painter();
            _highlighterPainter = new Painter
            {
                Color = Brushes.Yellow,
                StrokeWidth = (int)StrokeWidthEnum.UltraThick
            };
            _drawController = DrawController.GetDrawController();
        }

        public static DrawModel GetDrawModel()
        {
            return _drawModel ?? (_drawModel = new DrawModel());
        }

        public Page GetCurrentPage()
        {
            return PageList.GetCurrentPage();
        }

        public Page GetPage(int pageNumber)
        {
            if (pageNumber < 0 || pageNumber >= PageList.Count) return null;

            return PageList[pageNumber];
        }

        public Page GetPage(string pageTitle)
        {
            return PageList.FirstOrDefault(p => p.Title == pageTitle);
        }

        public Painter GetPainter()
        {
            return _currentPainter;
        }

        public Painter GetHighlighterPainter()
        {
            return _highlighterPainter;
        }

        public Painter GetCurrentlyUsedPainter()
        {
            if (_currentDrawObjectType == DrawObjectType.Highlighter)
                return _highlighterPainter;
            else
                return _currentPainter;
        }

        private void StopHighlighter()
        {
            _highlightUuid = null;
        }

        public DrawObject AddNewDrawObject(int id, Point pt)
        {
            DrawObject drawObject = null;
            if (_currentDrawObjectType != DrawObjectType.Highlighter)
                StopHighlighter();
            switch (_currentDrawObjectType)
            {
                case DrawObjectType.Highlighter:
                    if (_highlightUuid != null)
                    {
                        var lastDrawObject = GetCurrentPage().GetLastDrawObjectAdded();
                        if (lastDrawObject is Highlighter && 
                            lastDrawObject.Uuid == _highlightUuid && 
                            ((Highlighter)lastDrawObject).Painter == _highlighterPainter)
                        {
                            drawObject = lastDrawObject;
                            ((Highlighter)drawObject).AddNewStroke(pt, id);
                            GetCurrentPage().AddPointDown(id, drawObject);
                            return null;
                        }
                    }
                    drawObject = new Highlighter(new Painter(_highlighterPainter));
                    ((Highlighter)drawObject).AddNewStroke(pt, id);
                    _highlightUuid = drawObject.Uuid;
                    break;
                case DrawObjectType.Stroke:
                    drawObject = new Stroke(new Painter(_currentPainter));
                    break;
                case DrawObjectType.Rectangle:
                    drawObject = new Rectangle(new Painter(_currentPainter));
                    break;
                case DrawObjectType.Ellipse:
                    drawObject = new Ellipse(new Painter(_currentPainter));
                    break;
                case DrawObjectType.Selection:
                    drawObject = new DrawSelection();
                    break;
                case DrawObjectType.Line:
                    drawObject = new Line(new Painter(_currentPainter));
                    break;
                case DrawObjectType.Eraser:
                    drawObject = new Eraser();
                    break;
            }
            if (drawObject == null) return null;

            drawObject.AddPoint(pt, id);
            PageList.GetCurrentPage().AddPointDown(id, drawObject);

            if (drawObject is Eraser)
                EraseObjects(id, pt, (Eraser)drawObject);

            return drawObject;
        }

        public void CancelAllActiveDrawing()
        {
            GetCurrentPage().CancelAllDrawing();
        }

        public void AddPointUp(int id)
        {
            DrawObject lastObject = GetCurrentPage().GetLastDrawObjectAdded();
            if (lastObject is Eraser)
                _drawController.RemoveDrawObject(lastObject.Uuid);
            else if (lastObject is DrawSelection)
                SelectObjects(lastObject as DrawSelection);
        }

        public void SelectObjects(DrawSelection drawSelection)
        {
            if (drawSelection == null) return;
            IEnumerable<Tuple<DrawObject, List<int>>> selectedObjects = drawSelection.GetAllInSelectionPath(GetCurrentPage().GetAllDrawObjects().Where(o => !(o is DrawSelection || o is Eraser)));

            var highlighterFigToAdd = selectedObjects.Where(o => o.Item2 != null).ToList();
            List<DrawObject> drawObjectToAdd = selectedObjects.Where(o => o.Item2 == null).Select(o => o.Item1).ToList();

            foreach (var h in highlighterFigToAdd)
            {
                Highlighter highlighter = (Highlighter)h.Item1;

                drawObjectToAdd.Add(new Highlighter(highlighter.Painter, highlighter.GetPathFigures(h.Item2), highlighter.MatrixTransform));

                foreach (int id in h.Item2)
                    if (!highlighter.RemoveDraw(id))
                        RemoveDrawObject(h.Item1.Uuid);
            }

            if (drawObjectToAdd == null || !drawObjectToAdd.Any())
            {
                _drawController.RemoveDrawObject(drawSelection.Uuid);
                return;
            }
            _drawController.AddSelection(drawObjectToAdd);

            List<string> uuidList = drawObjectToAdd.Select(o => o.Uuid).ToList();
            uuidList.Add(drawSelection.Uuid);

            _drawController.RemoveDrawObject(uuidList);
        }

        public void EraseObjects(int id, Point pt, Eraser eraser)
        {
            if (eraser == null) return;
            IEnumerable<DrawObject> allDrawObjects = GetCurrentPage().GetAllDrawObjects().Where(o => !(o is DrawSelection || o is Eraser));
            IEnumerable<Tuple<DrawObject, int>> toErase = eraser.GetAllDrawObjectToErase(allDrawObjects);


            var highlighterToErase = toErase.Where(o => o.Item2 != -1).ToList();

            foreach (var highlighter in highlighterToErase)
                if (!((Highlighter)highlighter.Item1).RemoveDraw(highlighter.Item2))
                    RemoveDrawObject(highlighter.Item1.Uuid);

            List<string> uuidList = toErase.Where(o => o.Item2 == -1).Select(o => o.Item1.Uuid).ToList();

            if (highlighterToErase.Count == 0 && uuidList.Count == 0) return;
            
            RemoveDrawObject(eraser.Uuid);
            _drawController.RemoveDrawObject(uuidList);
            _drawController.AddPointDown(id, pt);
            //AddNewDrawObject(id, pt);
            //AddDrawObjectToCurrentPage(eraser);
        }


        public void AddDrawObjectToCurrentPage(DrawObject drawObject)
        {
            PageList.GetCurrentPage().AddInactiveObject(drawObject);
        }

        public void RemoveDrawObject(string uuid)
        {
            PageList.GetCurrentPage().RemoveDrawObject(uuid);
        }

        public void RemoveDrawObject(IEnumerable<string> uuidList)
        {
            PageList.GetCurrentPage().RemoveDrawObject(uuidList);
        }

        /// <summary>
        /// Undo last draw
        /// </summary>
        /// <returns>true if bitmap can be redrawn</returns>
        public bool UndoLastDraw()
        {
            DrawObject drawObject = GetLastDrawObjectAdded();
            if (drawObject == null) return false;
            if (!(drawObject is Highlighter) || !((Highlighter)drawObject).RemoveLastDraw())
            {
                PageList.GetCurrentPage().RemoveDrawObject(drawObject.Uuid);
                return true;
            }

            if (_highlightUuid != null && drawObject.Uuid == _highlightUuid)
                return false;

            return true;
        }

        public void AddPointMove(int id, Point pt)
        {
            PageList.GetCurrentPage().AddPointMove(id, pt);
        }

        public Page NewPage()
        {
            return NewPage("Slide " + (PageList.Count + 1));
        }

        public Page NewPage(string pageName)
        {
            Page page;

            if ((GetCurrentPage() != null) && (PageList.Count > 0) && (GetCurrentPage().IsPageEmpty()))
            {
                page = GetCurrentPage();
            }
            else
            {
                page = new Page();
                PageList.Add(page);
            }

            page.Title = pageName;
            _drawController.CreateThumbnail(page);
            
            return page;
        }

        public void AddPage(Page page)
        {
            PageList.Add(page);
            _drawController.CreateThumbnail(page);
        }

        public void RemoveAllPages()
        {
            PageList.Clear();
        }

        public void ResizeDrawing(Vector scale, Point center)
        {
            StopHighlighter();
            Page page = GetCurrentPage();
            page.Transform.ScaleAt(scale.X, scale.Y, center.X, center.Y);
            _drawController.RealTimeTransform(page.Transform);
        }

        public void MoveDrawing(Vector offset)
        {
            StopHighlighter();
            Page page = GetCurrentPage();
            page.Transform.Translate(offset.X, offset.Y);
            _drawController.RealTimeTransform(page.Transform);
        }

        public void ApplyMatrixToCurrentPage()
        {
            GetCurrentPage().ApplyMatrixToDrawObjects();
        }

        //public void RemovePage(Page page)
        //{
        //    if (pageList.Contains(page))
        //        pageList.Remove(page);
        //}

        public void NextPage()
        {
            StopHighlighter();
            Page oldPage = GetCurrentPage();

            Page page = PageList.GetNext();
            if (page != null)
                _drawController.NotifyPageChange(oldPage);
            else if (!GetCurrentPage().IsPageEmpty())
            {
                _drawController.NewPage();
                PageList.GetNext();
                _drawController.NotifyPageChange(oldPage);
            }
        }

        public void PreviousPage()
        {
            StopHighlighter();
            //if (pageList.GetCurrentPage().GetAllDrawObjects().Count == 0)
            //    RemovePage(pageList.GetCurrentPage());
            Page oldPage = GetCurrentPage();

            Page page = PageList.GetPrevious();
            if (page != null)
                _drawController.NotifyPageChange(oldPage);
        }

        public void ClearPage()
        {
            StopHighlighter();
            PageList.GetCurrentPage().ClearDrawObjects(true);
        }

        public void ChoosePage(int pageNbr)
        {
            StopHighlighter();
            Page oldPage = GetCurrentPage();

            Page page = PageList.Get(pageNbr);
            if (page != null)
                _drawController.NotifyPageChange(oldPage);
        }

        public void ChoosePage(Page page)
        {
            StopHighlighter();
            Page oldPage = GetCurrentPage();

            PageList.SetCurrent(page);
            _drawController.NotifyPageChange(oldPage);
        }

        public void SetCurrentDrawObjectType(DrawObjectType drawObjectType)
        {
            CurrentDrawObjectType = drawObjectType;
        }

        public DrawObjectType GetCurrentDrawObjectType()
        {
            return _currentDrawObjectType;
        }

		public DrawObject GetLastDrawObjectAdded()
        {
            return PageList.GetCurrentPage().GetLastDrawObjectAdded();
        }

        public void SetCurrentPageBackground(DrawObject drawObject)
        {
            SetBackgroundToPage(PageList.GetCurrentPage(), drawObject);
        }

        public void SetBackgroundToPage(Page page, DrawObject drawObject)
        {
            page.Background = drawObject;
        }

        /// <summary>
        /// Resize the background to take the maximum place into the specified area
        /// </summary>
        /// <param name="size">Size of the area</param>
        /// <param name="page">page where to resize the background, current page if null or not specified</param>
        public void ResizeBackgroundToCenter(Size size, Page page = null)
        {
            if (page == null)
                page = PageList.GetCurrentPage();

            page.ResizeBackgroundToCenter(size);
        }


        /// <summary>
        /// Get SVG of a specific page. If no page is given then gets the current page
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public string GetSVG(Page page = null)
        {
            if (page == null)
                page = GetCurrentPage();
            Rect? pageArea = page.GetBoundingBox();
            if (pageArea == null) return null;

            XDocument svgDocument = new XDocument();
            XNamespace xmlns = "http://www.w3.org/2000/svg";
            XElement svgMain = new XElement(xmlns + "svg",
                new XAttribute("width", ((int)pageArea.Value.Width).ToString()),
                new XAttribute("height", ((int)pageArea.Value.Height).ToString())
                //new XNamespace("xmln","http://www.w3.org/2000/svg")
                );

            Matrix transform = new Matrix();
            transform.Translate(-pageArea.Value.Left, -pageArea.Value.Top);

            if (page.Background != null)
            { 
                
                XElement element = page.Background.GetSVG(xmlns, transform);
                if (element != null)
                    svgMain.Add(element);
            }

            foreach (DrawObject obj in page.GetAllDrawObjects())
            {
                XElement element = obj.GetSVG(xmlns, transform);
                if (element != null)
                    svgMain.Add(element);
            }

            svgDocument.Add(svgMain);

            return svgDocument.ToString();
        }


        public List<string> GetAllSVG()
        {
            List<string> allSvg = new List<string>();
            foreach (Page page in PageList)
                allSvg.Add(GetSVG(page));

            return allSvg;
        }


        public string Serialize()
        {
            return JsonConvert.SerializeObject(PageList, Formatting.None, JsonUtils.SerializeConverter);
        }

        public bool Deserialize(string serializedPages, PDFViewer pdfViewer)
        {
            try
            {
                RemoveAllPages();
                PageList = JsonConvert.DeserializeObject<PageList>(serializedPages, JsonUtils.DeserializeConverters);

                //I don't know yet how to pass parameter to the deserializer callback (only when it is a DrawPDF instance)
                // so I replace the deserialied drawPDF by a new one with the PDFViewr 
                if (pdfViewer != null)
                {
                    foreach (Page page in PageList)
                    {
                        DrawPDF drawPdf = (DrawPDF) page.Background;
                        if (drawPdf != null)
                        {
                            page.Background = new DrawPDF(pdfViewer, drawPdf.Page)
                            {
                                MatrixTransform = drawPdf.MatrixTransform
                            };
                            //page.FirstBackgroundResizeDone = true;
                        }
                    }
                }
            }
            catch (JsonReaderException)
            {
                return false;
            }
            return _drawController.GetCurrentPage() != null;
        }
    }
}
