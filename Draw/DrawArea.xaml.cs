﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using TouchAndTeachWindows.Draw.DrawObjects;

namespace TouchAndTeachWindows.Draw
{
    /// <summary>
    /// Interaction logic for DrawArea.xaml
    /// </summary>
    public partial class DrawArea : UserControl
    {
        private const int MaxDrawobjectNumber = 10;
        private readonly DrawController _drawController;
        //private RenderTargetBitmap drawedPathsBitmap;
        private readonly Random _rand;
        // Touch on Windows doesn't use random id, so the id of a new touch can easily be one that has been used already. But never of one being in use.
        // This is why we need to generate random id and access those with a dictionnary of the id actualy in use.
        private readonly Dictionary<int, int> _activeId;
        // list to make sure we never have 2 times the same id
        private readonly List<int> _listId;
        private readonly DrawingGroup _drawingGroup;
        private GeometryDrawing _transparentRectangle;
        private static DrawArea _drawArea;
        private Rect _drawingSize;
        private DateTime _lastTransform;
        private Point _lastMousePos = new Point(int.MaxValue, int.MaxValue);

        Task _checkLastTransformTask;

        private DrawArea()
        {
            InitializeComponent();

            _drawController = DrawController.GetDrawController();
            _rand = new Random();
            _activeId = new Dictionary<int,int>();
            _listId = new List<int>();
            _drawingGroup = new DrawingGroup();
            RenderedImage.Source = new DrawingImage(_drawingGroup);
            Application.Current.Windows.OfType<MainWindow>().First().SizeChanged += DrawingArea_SizeChanged;
        }

        public static DrawArea GetDrawArea()
        {
            return _drawArea ?? (_drawArea = new DrawArea());
        }

        public void ClearScreen()
        {
            DrawingArea.Children.Clear();
        }

        public void RewriteBitmap()
        {
            if (_drawController.PauseDrawing) return;

            DrawingArea.Children.Clear();
            _drawingGroup.Children.Clear();
            //_activeId.Clear();

            DrawingVisual visual = new DrawingVisual();
            Image tempImage = new Image
            {
                Stretch = Stretch.None,
                VerticalAlignment = VerticalAlignment.Top,
                HorizontalAlignment = HorizontalAlignment.Left
            };
            RenderTargetBitmap bitmap = new RenderTargetBitmap((int)_drawingSize.Width, (int)_drawingSize.Height, 96, 96, PixelFormats.Pbgra32);

            DrawingArea.Children.Add(tempImage);

            using (DrawingContext drawingContext = visual.RenderOpen())
            {
                drawingContext.DrawDrawing(_transparentRectangle);

                // Get Page to draw
                Page pageToDraw = _drawController.GetCurrentPage();

                if (pageToDraw != null)
                {
                    if (!pageToDraw.FirstBackgroundResizeDone)
                        _drawController.ResizeBackgroundToCenter(pageToDraw);

                    //background
                    DrawObject background = pageToDraw.Background;
                    if (background != null)
                    {
                        background.AddToDrawingContext(drawingContext);
                        drawingContext.Pop();
                    }
                    //all drawObjects
                    foreach (DrawObject drawObject in pageToDraw.GetAllDrawObjects().Where(o => !(o is DrawSelection || o is Eraser)))
                    {
                        if (drawObject != null)
                        {
                            drawObject.AddToDrawingContext(drawingContext);
                            drawingContext.Pop();
                        }
                    }
                }
            }

            bitmap.Render(visual);
            tempImage.Source = bitmap;
            visual = null;

            DrawingArea.Children.Add(RenderedImage);

            using (DrawingContext dc = _drawingGroup.Open())
            {
                dc.DrawDrawing(_transparentRectangle);
            }
        }

        public void AddToBitmap(List<DrawObject> drawObjects)
        {
            RewriteBitmap();
            //Rect? boundingBox = drawController.GetCurrentPage().GetBoundingBox();

            //if (boundingBox != null)
            //{
            //    Rect imageSize = new Rect(new Point(), new Point(boundingBox.Value.Right + 100, boundingBox.Value.Bottom + 100));

            //    drawedPathsBitmap = new RenderTargetBitmap((int)imageSize.Width, (int)imageSize.Height, 96, 96, PixelFormats.Pbgra32);

            //    if (renderedImage.Height != imageSize.Height || renderedImage.Width != imageSize.Width)
            //    {
            //        RewriteBitmap();
            //        drawedPathsBitmap = new RenderTargetBitmap((int)imageSize.Width, (int)imageSize.Height, 96, 96, PixelFormats.Pbgra32);
            //        //drawedPathsBitmap = new RenderTargetBitmap((int)DrawingArea.ActualWidth, (int)DrawingArea.ActualHeight, 96, 96, PixelFormats.Pbgra32);
            //        renderedImage = new Image();
            //        renderedImage.Width = imageSize.Width;
            //        renderedImage.Height = imageSize.Height;

            //        renderedImage.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            //        renderedImage.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            //        renderedImage.Source = drawedPathsBitmap;
            //        DrawingArea.Children.Add(renderedImage);
            //    }

            //    foreach (DrawObject drawObject in drawObjects)
            //    {
            //        if (DrawingArea.Children.Contains(drawObject.shape))
            //            DrawingArea.Children.Remove(drawObject.shape);
            //        drawedPathsBitmap.Render(drawObject.shape);
            //    }
            //}
        }

        public void RequestSaveToBitmap()
        {
            List<DrawObject> drawObjects = _drawController.GetCurrentPage().GetNotInBitmapDrawObjects();
            if (drawObjects.Count >= MaxDrawobjectNumber)
            {
                AddToBitmap(drawObjects);
                _drawController.GetCurrentPage().ClearDrawObjects(false);
            }
        }

        private void DrawingArea_TouchDown(object sender, TouchEventArgs e)
        {
            TouchPoint pos = e.GetTouchPoint(DrawingArea);
            // Generate random id
            int id = _rand.Next(Int32.MaxValue);
            while (_listId.Contains(id))
                id = _rand.Next(Int32.MaxValue);
           
            if (!_activeId.ContainsKey(pos.TouchDevice.Id))
            {
                _activeId.Add(pos.TouchDevice.Id, id);
                _listId.Add(id);

                _drawController.AddPointDown(id, pos.Position);
            }
            //e.Handled = true;
        }

        private void DrawingArea_TouchMove(object sender, TouchEventArgs e)
        {
            TouchPoint pos = e.GetTouchPoint(DrawingArea);
            if (_activeId.ContainsKey(pos.TouchDevice.Id))
                _drawController.AddPointMove(_activeId[pos.TouchDevice.Id], pos.Position);
            //e.Handled = true;
        }

        private void DrawingArea_TouchUp(object sender, TouchEventArgs e)
        {
            if (_activeId.ContainsKey(e.TouchDevice.Id))
            {
                _drawController.AddPointUp(_activeId[e.TouchDevice.Id]);
                _activeId.Remove(e.TouchDevice.Id);
                RequestSaveToBitmap();
            }
            e.Handled = true;
        }

        public void AddDrawObject(DrawObject drawObject)
        {
            using (DrawingContext drawingContext = _drawingGroup.Append())
            {
                drawObject.AddToDrawingContext(drawingContext);
            }
        }

        //public void RemoveDrawObject(DrawObject drawObject)
        //{
        //    var drawing = _drawingGroup.Children.OfType<DrawingGroup>().FirstOrDefault(dg => dg.Children.Count > 0 && ((GeometryDrawing)dg.Children[0]).Geometry == drawObject.GetGeometry(false));
        //    if (drawing != null)
        //        _drawingGroup.Children.Remove(drawing);
        //}


        private void DrawingArea_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!_drawController.ZoomMode)
            {
                Point pos = e.GetPosition(DrawingArea);
                int id = _rand.Next(Int32.MaxValue);
                while (_listId.Contains(id))
                    id = _rand.Next(Int32.MaxValue);

                // -1 is mouse id
                if (!_activeId.ContainsKey(-1))
                {
                    _activeId.Add(-1, id);
                    _listId.Add(id);

                    _drawController.AddPointDown(id, pos);
                }
            }
            else
                _lastMousePos = e.GetPosition(this);
            e.Handled = true;
        }

        private void DrawingArea_MouseMove(object sender, MouseEventArgs e)
        {
            if (!_drawController.ZoomMode && _activeId.ContainsKey(-1) && e.LeftButton == MouseButtonState.Pressed)
            {
                Point pos = e.GetPosition(DrawingArea);
                _drawController.AddPointMove(_activeId[-1], pos);
            }
            else if (_drawController.ZoomMode && _lastMousePos.X < int.MaxValue)
            {
                Point pos = e.GetPosition(this);
                _drawController.MoveDrawing(pos - _lastMousePos);
                _lastMousePos = pos;
            }
            e.Handled = true;
        }

        private void DrawingArea_MouseLeftButtonUp(object sender, MouseEventArgs e)
        {
            if (_activeId.ContainsKey(-1))
            {
                _drawController.AddPointUp(_activeId[-1]);
                _activeId.Remove(-1);
                //RequestSaveToBitmap();
            }
            _lastMousePos = new Point(int.MaxValue, int.MaxValue);
            e.Handled = true;
        }

        private void DrawingArea_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (!_drawController.ZoomMode) return;

            _drawController.ResizeDrawing(e.Delta > 0 ? new Vector(1.1, 1.1) : new Vector(0.9, 0.9), e.GetPosition(DrawingArea));
        }


        private void DrawingArea_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (!(e.NewSize.Height > _drawingSize.Height) && !(e.NewSize.Width > _drawingSize.Width)) return;

            Rect bound = _drawingGroup.Bounds;
            Rect newSize = new Rect(e.NewSize);
            if (!bound.IsEmpty)
            {
                newSize = new Rect(bound.TopLeft, newSize.BottomRight);
            }

            _drawingSize = new Rect(new Point(Math.Min(newSize.Left, _drawingSize.Left), Math.Min(newSize.Top, _drawingSize.Top)), 
                new Point(Math.Max(newSize.Right, _drawingSize.Right), Math.Max(newSize.Bottom, _drawingSize.Bottom)));

            _transparentRectangle = new GeometryDrawing(Brushes.Transparent, null, new RectangleGeometry(_drawingSize));
            using (DrawingContext dc = _drawingGroup.Append())
            {
                dc.DrawDrawing(_transparentRectangle);
            }

            Matrix matrix = RenderedImage.RenderTransform.Value;
            matrix.OffsetX = _drawingSize.Left;
            matrix.OffsetY = _drawingSize.Top;
            RenderedImage.RenderTransform = new MatrixTransform(matrix);
            DrawingArea.Width = _drawingSize.Width;
            DrawingArea.Height = _drawingSize.Height;
        }

        private void UserControl_ManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            if (_drawController.ZoomMode && (e.DeltaManipulation.Translation.LengthSquared > 50 || (Math.Abs(e.DeltaManipulation.Scale.LengthSquared - 2) > double.Epsilon)))
            {
                _drawController.ResizeDrawing(e.DeltaManipulation.Scale, e.ManipulationOrigin);
                _drawController.MoveDrawing(e.DeltaManipulation.Translation);
            }
        }

        public void TransformDrawing(Matrix matrix)
        {
#warning put on comment to improve resizing with pdf, but to check if it doesn't bring any other issue
            //if (_drawingGroup.Children.Count > 1)
            //    RewriteBitmap();

            Matrix imgMatrix = DrawingArea.RenderTransform.Value;
            imgMatrix.Append(matrix);
            DrawingArea.RenderTransform = new MatrixTransform(matrix);
            _lastTransform = DateTime.UtcNow;
            if (_checkLastTransformTask == null || _checkLastTransformTask.IsCompleted)
                _checkLastTransformTask = Task.Factory.StartNew(CheckLastTransform);
        }

        public void EndTransform()
        {
            DrawingArea.RenderTransform = new MatrixTransform();
            RewriteBitmap();
        }

        private void CheckLastTransform()
        {
            while (_lastTransform.AddMilliseconds(200).Ticks > DateTime.UtcNow.Ticks)
            {
                Task.Delay(20);
            }

            _drawController.FinalizeMoveAndResizeDrawing();
        }
    }
}
