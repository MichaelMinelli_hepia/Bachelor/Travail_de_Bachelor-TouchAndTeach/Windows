﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;
using System.Windows.Threading;
using DocToWPF;
using TouchAndTeachWindows.Draw.DrawObjects;
using TouchAndTeachWindows.Draw.SelectionUI;
using TouchAndTeachWindows.Draw.UserDrawingUI;
using System.ComponentModel;

namespace TouchAndTeachWindows.Draw
{
    public class DrawController : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = (obj, arg) => { };

        private DrawArea _drawArea;
        private DrawModel _drawModel;
        private static DrawController _drawController;
        private Matrix _imageZoom;

        private bool _zoomMode = false;
        public bool ZoomMode
        {
            get { return _zoomMode; }
            set
            {
                if (_zoomMode == value) return;
                _zoomMode = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ZoomMode"));
            }
        }

        public Boolean PauseDrawing = false;

        public Dictionary<Page, Thumbnail> Thumbnails
        {
            get;
            private set;
        }
        
        private int _width;
        private int _height;

        private Dispatcher _dispatcher;

        public event Action<Page> ThumbnailAdded = page => { };
        public event Action<int, Point> NewPointDown = (int id, Point pos) => { };
        public event Action<UserDrawing> UserDrawingAdded = (userDrawing) => { };
        public event Action<Selection> SelectionAdded = (selection) => { };
        

        //public event Action<Thumbnail> ThumbnailRemoving = (thumbnail) => { };

        private DrawController()
        {
            ZoomMode = false;
            Thumbnails = new Dictionary<Page, Thumbnail>();
        }

        public void Init(Window window)
        {
            _drawModel = DrawModel.GetDrawModel();
            _drawArea = DrawArea.GetDrawArea();
            _dispatcher = window.Dispatcher;
        }

        public static DrawController GetDrawController()
        {
            return _drawController ?? (_drawController = new DrawController());
        }


        private Object ExecuteThreadSafeAction(Func<Object> action)
        {
            object returnVal = Thread.CurrentThread != _dispatcher.Thread ? _dispatcher.Invoke(action) : action();
            return returnVal;
        }

        private dynamic ExecuteThreadSafeActionDyn(Func<dynamic> action)
        {
            object returnVal = Thread.CurrentThread != _dispatcher.Thread ? _dispatcher.Invoke(action) : action();
            return returnVal;
        }

        public void AddPointDown(int id, Point pos)
        {
            Func<Object> action = () =>
                {
                    if (!ZoomMode)
                    {
                        DrawObject drawObject = _drawModel.AddNewDrawObject(id, pos);
                        if (drawObject != null)
                        {
                            _drawArea.AddDrawObject(drawObject);
                            NewPointDown(id, pos);
                        }
                    }
                    return null;
                };

            ExecuteThreadSafeAction(action);
        }

        public void AddPointMove(int id, Point pos)
        {
            Func<Object> action = () =>
                {
                    if (!ZoomMode)
                        _drawModel.AddPointMove(id, pos);
                    return null;
                };

            ExecuteThreadSafeAction(action);
        }

        public void AddPointUp(int id)
        {
            Func<Object> action = () =>
            {
                if (!ZoomMode)
                    _drawModel.AddPointUp(id);
                return null;
            };

            ExecuteThreadSafeAction(action);
        }

        public void NextPage()
        {
            Func<Object> action = () =>
                {
                    _drawModel.NextPage();
                    return null;
                };

            ExecuteThreadSafeAction(action);
        }

        public void PreviousPage()
        {
            Func<Object> action = () =>
                {
                    _drawModel.PreviousPage();
                    return null;
                };

            ExecuteThreadSafeAction(action);
        }

        public void ChoosePage(int pageNbr)
        {
            Func<Object> action = () =>
                {
                    _drawModel.ChoosePage(pageNbr);
                    return null;
                };

            ExecuteThreadSafeAction(action);
        }

        public void ChoosePage(Page page)
        {
            Func<Object> action = () =>
                {
                    Page oldPage = GetCurrentPage();
                    _drawModel.ChoosePage(page);
                    NotifyPageChange(oldPage);
                    return null;
                };


            ExecuteThreadSafeAction(action);
        }

        public Page NewPage()
        {
            Func<Page> action = () =>_drawModel.NewPage();
            return (Page)ExecuteThreadSafeAction(action);
        }

        public Page NewPage(string pageTitle)
        {
            Func<Page> action = () => _drawModel.NewPage(pageTitle);
            return (Page)ExecuteThreadSafeAction(action);
        }

        public void AddPage(Page page)
        {
            Func<Object> action = () =>
            {
                if (page != null)
                {
                    _drawModel.AddPage(page);
                }
                return null;
            };

            ExecuteThreadSafeAction(action);
        }

        public void RemoveAllPages()
        {
            Func<Object> action = () =>
            {
                Thumbnails.Clear();
                MainWindow mainWindow = (MainWindow)Application.Current.MainWindow;
                mainWindow.Menu.ThumbnailsList.RemoveAllElements();
                _drawModel.RemoveAllPages();
                App.DisposeAll();
                return null;
            };

            ExecuteThreadSafeAction(action);
        }

        public void CreateThumbnail(Page page)
        {
            Func<Object> action = () =>
            {
                if (page != null)
                {
                    if (!Thumbnails.ContainsKey(page))
                    {
                        Thumbnails.Add(page, new Thumbnail(page, (int)_drawArea.ActualWidth, (int)_drawArea.ActualHeight));
                        ThumbnailAdded(page);
                    }
                    else
                        RefreshThumbnail(page);
                }
                return null;
            };

            ExecuteThreadSafeAction(action);
        }

        //public void RemovePage(Page page)
        //{
        //    if (page != null)
        //    {
        //        drawModel.RemovePage(page);
        //        ThumbnailRemoving(thumbnails[page]);
        //        if (thumbnails.ContainsKey(page))
        //            thumbnails.Remove(page);
        //    }

        //}

        public Page GetCurrentPage()
        {
            Func<Page> action = () => _drawModel.GetCurrentPage();
            return (Page)ExecuteThreadSafeAction(action);
        }

        public void NotifyPageChange(Page previousPage = null)
        {
            //List<Page> toRemove = new List<Page>();
            //foreach (Page page in drawModel.pageList)
            //{
            //    if (page.GetAllDrawObjects().Count == 0 && page != drawModel.GetCurrentPage())
            //        toRemove.Add(page);
            //}
            //foreach (Page page in toRemove)
            //    RemovePage(page);

            Func<Object> action = () =>
                {
                    _drawArea.RewriteBitmap();
                    if (previousPage != null)
                        RefreshThumbnail(previousPage);

                    Page actualPage = GetCurrentPage();
                    if (!Thumbnails.ContainsKey(actualPage))
                    {
                        Thumbnails.Add(actualPage, new Thumbnail(actualPage, (int)_drawArea.ActualWidth, (int)_drawArea.ActualHeight));
                        ThumbnailAdded(actualPage);
                    }

                    return null;
                };
            ExecuteThreadSafeAction(action);
        }

        public void ClearPage()
        {
            Func<Object> action = () =>
                {
                    _drawArea.ClearScreen();
                    _drawModel.ClearPage();
                    //_drawModel.GetPainter().Mode = Painter.PainterMode.Crayon;
                    return null;
                };

            ExecuteThreadSafeAction(action);
        }

        public void Undo()
        {
            Func<Object> action = () =>
            {
                if (_drawModel.UndoLastDraw())
                    _drawArea.RewriteBitmap();
                return null;
            };

            ExecuteThreadSafeAction(action);
        }

        public void AddDrawObjectToCurrentPage(DrawObject obj)
        {
            Func<Object> action = () =>
            {
                _drawModel.AddDrawObjectToCurrentPage(obj);
                _drawArea.AddDrawObject(obj);
                return null;
            };
            ExecuteThreadSafeAction(action);
        }

        public void RemoveDrawObject(string uuid)
        {
            Func<Object> action = () =>
            {
                _drawModel.RemoveDrawObject(uuid);
                _drawArea.RewriteBitmap();
                return null;
            };
            ExecuteThreadSafeAction(action);
        }

        public void RemoveDrawObject(IEnumerable<string> uuidArray)
        {
            Func<Object> action = () =>
            {
                _drawModel.RemoveDrawObject(uuidArray);
                _drawArea.RewriteBitmap();
                return null;
            };
            ExecuteThreadSafeAction(action);
        }

        public void CancelAllActiveDrawing()
        {
            Func<Object> action = () =>
            {
                _drawModel.CancelAllActiveDrawing();
                return null;
            };
            ExecuteThreadSafeAction(action);
        }

        public void RefreshDrawing()
        {
            Func<Object> action = () =>
            {
                _drawArea.RewriteBitmap();
                return null;
            };
            ExecuteThreadSafeAction(action);
        }

        public void ResizeDrawing(Vector scale, Point center)
        {
            Func<Object> action = () =>
                {
                    _drawModel.ResizeDrawing(scale, center);
                    return null;
                };

            ExecuteThreadSafeAction(action);
        }

        public void MoveDrawing(Vector offset)
        {
            Func<Object> action = () =>
                {
                    _drawModel.MoveDrawing(offset);
                    return null;
                };

            ExecuteThreadSafeAction(action);
        }

        public void FinalizeMoveAndResizeDrawing()
        {
            Func<Object> action = () =>
                {
                    _drawModel.ApplyMatrixToCurrentPage();
                    _drawArea.EndTransform();
                    return null;
                };

            ExecuteThreadSafeAction(action);
        }

        public void RealTimeTransform(Matrix matrix)
        {
            Func<Object> action = () =>
                {
                    _drawArea.TransformDrawing(matrix);
                    return null;
                };

            ExecuteThreadSafeAction(action);
        }

        public void SetDrawObjectStyle(DrawModel.DrawObjectType type)
        {
            Func<Object> action = () =>
                {
                    _drawModel.SetCurrentDrawObjectType(type);
                    return null;
                };
            ExecuteThreadSafeAction(action);
        }

        public DrawModel.DrawObjectType GetDrawObjectType()
        {
            Func<Object> action = () => _drawModel.GetCurrentDrawObjectType();
            return (DrawModel.DrawObjectType)ExecuteThreadSafeAction(action);
        }

        public Painter GetPainter()
        {
            Func<Painter> action = () => _drawModel.GetPainter();
            return (Painter)ExecuteThreadSafeAction(action);
        }

        public Painter GetHighlighterPainter()
        {
            Func<Painter> action = () => _drawModel.GetHighlighterPainter();
            return (Painter)ExecuteThreadSafeAction(action);
        }

        /// <summary>
        /// Will return the painter in use. If Highlighter is selected then return highlighter's, otherwise the main one's.
        /// </summary>
        /// <returns></returns>
        public Painter GetCurrentlyUsedPainter()
        {
            Func<Painter> action = () => _drawModel.GetCurrentlyUsedPainter();
            return (Painter)ExecuteThreadSafeAction(action);
        }

        public void RefreshAllThumbnails()
        {
            Func<Object> action = () =>
                {
                    _drawArea.RewriteBitmap();
                    foreach (Page page in _drawModel.PageList)
                    {
                        if (!Thumbnails.ContainsKey(page))
                        {
                            Thumbnails.Add(page, new Thumbnail(page, (int)_drawArea.ActualWidth, (int)_drawArea.ActualHeight));
                            ThumbnailAdded(page);
                        }
                        else
                            Thumbnails[page].Refresh();
                    }
                    return null;
                };
            ExecuteThreadSafeAction(action);
        }

        public void RefreshThumbnail(Page page)
        {
            Func<Object> action = () =>
            {
                Thumbnail thumbnail;
                Thumbnails.TryGetValue(page, out thumbnail);
                if (thumbnail != null)
                {
                    Thumbnails[page].Refresh();
                }
                return null;
            };
            ExecuteThreadSafeAction(action);
        }

        public void AddUserDrawing(Page page)
        {
            Func<Object> action = () =>
                {
                    UserDrawing userDrawing = new UserDrawing(page);
                    UserDrawingAdded(userDrawing);
                    return null;
                };
            ExecuteThreadSafeAction(action);
        }

        public void AddSelection(List<DrawObject> drawObjects)
        {
            Func<Object> action = () =>
            {
                Selection selection = new Selection(drawObjects);
                SelectionAdded(selection);
                return null;
            };
            ExecuteThreadSafeAction(action);
        }

        public void SetCurrentPageBackground(DrawObject drawObject)
        {
            Func<Object> action = () =>
            {
                _drawModel.SetCurrentPageBackground(drawObject);
                RefreshDrawing();
                return null;
            };
            ExecuteThreadSafeAction(action);
        }

        public void SetBackgroundToPage(Page page, DrawObject drawObject)
        {
            Func<Object> action = () =>
            {
                _drawModel.SetBackgroundToPage(page, drawObject);
                return null;
            };
            ExecuteThreadSafeAction(action);
        }

        /// <summary>
        /// Resize the background to take the maximum place into the specified area
        /// </summary>
        /// <param name="page">page where to resize the background, current page if null or not specified</param>
        public void ResizeBackgroundToCenter(Page page = null)
        {
            Func<Object> action = () =>
            {
                _drawModel.ResizeBackgroundToCenter(new Size(_drawArea.ActualWidth, _drawArea.ActualHeight), page);
                return null;
            };
            ExecuteThreadSafeAction(action);
        }

        public Size GetAreaSize()
        {
            Func<dynamic> action = () => new Size(_drawArea.ActualWidth, _drawArea.ActualHeight);
            return (Size)ExecuteThreadSafeActionDyn(action);
        }

        public string GetSVG(Page page = null)
        {
            Func<string> action = () => _drawModel.GetSVG(page);
            return (string)ExecuteThreadSafeAction(action);
        }

        public List<string> GetAllSVG()
        {
            Func<List<string>> action = () => _drawModel.GetAllSVG();
            return (List<string>)ExecuteThreadSafeAction(action);
        }


        public string Serialize()
        {
            Func<string> action = () => _drawModel.Serialize();
            return (string)ExecuteThreadSafeAction(action);
        }

        public bool Deserialize(string serializedPages, PDFViewer pdfViewer)
        {
            Func<Object> action = () =>
                {
                    //delete the thumbnails. TODO They should be databinded to remove manual sync between Thumnails dictionnary and Menu.ThumbnailList
                    Thumbnails.Clear();
                    MainWindow mainWindow = (MainWindow) Application.Current.MainWindow;
                    mainWindow.Menu.ThumbnailsList.RemoveAllElements();

                    bool status = _drawModel.Deserialize(serializedPages, pdfViewer);
                    ChoosePage(0);
                    return status;
                };
            return (bool)ExecuteThreadSafeAction(action);
        }
    }
}
