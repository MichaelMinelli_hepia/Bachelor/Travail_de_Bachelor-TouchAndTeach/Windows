﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using TouchAndTeachWindows.Draw.DrawObjects;

namespace TouchAndTeachWindows.Draw.UserDrawingUI
{
    /// <summary>
    /// Interaction logic for UserDrawing.xaml
    /// </summary>
    public partial class UserDrawing : IDisposable
    {
        public enum UserDrawingMode
        {
            Postit,
            Transparent
        };

        private static readonly string[] PostitList =
        {
            "postit_azur.png",
            "postit_bleu.png",
            "postit_brun.png",
            "postit_eco.png",
            "postit_rose.png",
            "postit_vert.png",
            "postit_violet.png"
        };
        private Page _page;
        private DrawingGroup _drawingGroup;
        private GeometryDrawing _transparentRectangle;
        private readonly TransformGroup _transformGroup = new TransformGroup();
        private Point? _mouseLastPos;
        private Matrix _manipulationMatrix;
        private Matrix _contentMatrix;
        private Boolean _manipulated;
        private const int PostitSize = 500;
        private double _scale;
        private RenderTargetBitmap _bitmap;
        private BitmapImage _postitImage;
        private UserDrawingMode _selectedMode = UserDrawingMode.Postit;

        public UserDrawing(Page page)
        {
            _page = page;

            Rect? boundingBox = _page.GetBoundingBox();
            if (boundingBox != null)
            {
                Width = PostitSize;
                Height = PostitSize;
                _transparentRectangle = new GeometryDrawing(Brushes.Transparent, null, new RectangleGeometry(boundingBox.Value));
            }

            RenderTransform = new MatrixTransform();

            InitializeComponent();
            ShowDrawing();
        }

        public UserDrawingMode SelectedUserDrawingMode
        {
            get { return _selectedMode;  }
            set
            {
                _selectedMode = value;
                switch (_selectedMode)
                {
                    case UserDrawingMode.Postit:
                        PostitImage.Source = _postitImage;
                        Border.BorderThickness = new Thickness(0);
                        break;
                    case UserDrawingMode.Transparent:
                        PostitImage.Source = null;
                        Border.BorderThickness = new Thickness(1);
                        break;
                }
            }
        }

        public void MergeUserDrawing()
        {
            if (_page != null)
            {
                DrawController controller = DrawController.GetDrawController();
                Matrix matrix = new Matrix();
                matrix.Append(_contentMatrix);
                matrix.Append(_manipulationMatrix);

                if (_page.Background != null)
                {
                    _page.Background.MatrixTransform = matrix;
                    controller.AddDrawObjectToCurrentPage(_page.Background);
                }

                foreach (DrawObject drawObject in _page.GetAllDrawObjects())
                {
                    if (drawObject != null)
                    {
                        drawObject.MatrixTransform.Append(matrix);
                        controller.AddDrawObjectToCurrentPage(drawObject);
                    }
                }
            }
            Dispose();
        }

        private void ShowDrawing()
        {
            _drawingGroup = new DrawingGroup();
            if ( _transparentRectangle == null) return;

            _drawingGroup.Children.Add(_transparentRectangle);

            //postit random color
            Random rnd = new Random();
            _postitImage = new BitmapImage(new Uri("/TouchAndTeachWindows;component/ImgRes/Postit/" + PostitList[rnd.Next(7)], UriKind.Relative));
            PostitImage.Source = _postitImage;

            if (_page == null) return;

            //create bitmap from page, it fills the UserDrawing
            Rect? bound = _page.GetBoundingBox();
            Point pos = new Point();
            if (bound != null)
            {
                if (bound.Value.Width < bound.Value.Height)
                    _scale = PostitSize / bound.Value.Height;
                else
                    _scale = PostitSize / bound.Value.Width;
                pos = new Point((PostitSize / _scale - bound.Value.Width) / 2 - bound.Value.Left,
                    (PostitSize / _scale - bound.Value.Height) / 2 - bound.Value.Top);
            }

            _bitmap = new RenderTargetBitmap(PostitSize, PostitSize, 96, 96, PixelFormats.Pbgra32);
            DrawingVisual drawingVisual = new DrawingVisual();

            using (DrawingContext drawingContext = drawingVisual.RenderOpen())
            {
                _contentMatrix = new Matrix();
                _contentMatrix.Translate(pos.X, pos.Y);
                _contentMatrix.Scale(_scale, _scale);
                _contentMatrix.ScaleAt(0.7, 0.7, PostitSize/2, PostitSize/2);
                drawingContext.PushTransform(new MatrixTransform(_contentMatrix));

                DrawObject background = _page.Background;
                if (background != null)
                {
                    background.AddToDrawingContext(drawingContext);
                    drawingContext.Pop();
                }

                foreach (DrawObject drawObject in _page.GetAllDrawObjects())
                {
                    if (drawObject != null)
                    {
                        drawObject.AddToDrawingContext(drawingContext);
                        drawingContext.Pop();
                    }
                }
            }
            _bitmap.Render(drawingVisual);
            RenderedImage.Source = _bitmap;
            SelectedUserDrawingMode = UserDrawingMode.Postit;
        }

        private void UserControl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _mouseLastPos = e.GetPosition((Panel)Parent);
        }

        private void UserControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            _mouseLastPos = null;
        }

        private void UserControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (_mouseLastPos == null) return;

            Point newPos = e.GetPosition((Panel)Parent);
            _manipulationMatrix.Translate(newPos.X - _mouseLastPos.Value.X, newPos.Y - _mouseLastPos.Value.Y);
            RenderTransform = new MatrixTransform(_manipulationMatrix);
            _mouseLastPos = newPos;
        }

        private void UserControl_MouseLeave(object sender, MouseEventArgs e)
        {
            _mouseLastPos = null;
        }

        private void UserDrawingMenuButton_OnTouchDown(object sender, TouchEventArgs e)
        {
            Window window = Application.Current.MainWindow;
            Grid grid = (Grid)window.Content;
            Point position = e.GetTouchPoint(grid).Position;
            OpenUserDrawingMenu(position, e);
        }

        private void UserDrawingMenuButton_OnLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Window window = Application.Current.MainWindow;
            Grid grid = (Grid)window.Content;
            Point position = e.GetPosition(grid);
            OpenUserDrawingMenu(position, e);
        }

        private void OpenUserDrawingMenu(Point position, InputEventArgs inputArg)
        {
            MainWindow mainWindow = (MainWindow)Application.Current.MainWindow;
            if (mainWindow == null) return;
            mainWindow.AddUserDrawingMenu(this, position);
        }

        public void Dispose()
        {
            Panel panel = (Panel)Parent;
            if (panel != null)
            {
                UIElementCollection children = panel.Children;
                if (children != null)
                    children.Remove(this);
            }
            _drawingGroup = null;
            _transparentRectangle = null;
            _mouseLastPos = null;
            _page = null;
        }

        private void UserControl_ManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            if (_mouseLastPos != null ||
                (!_manipulated &&
                (!(e.DeltaManipulation.Translation.LengthSquared > 50)) &&
                (!(Math.Abs(e.DeltaManipulation.Scale.LengthSquared - 2) > double.Epsilon)) &&
                (!(Math.Abs(e.DeltaManipulation.Rotation) > double.Epsilon)))) return;

            _manipulated = true;
            _manipulationMatrix.RotateAt(e.DeltaManipulation.Rotation, e.ManipulationOrigin.X, e.ManipulationOrigin.Y);

            //Matrix matrixTemp = new Matrix(_manipulationMatrix.M11, _manipulationMatrix.M12, _manipulationMatrix.M21, _manipulationMatrix.M22, _manipulationMatrix.OffsetX, _manipulationMatrix.OffsetY);

            _manipulationMatrix.ScaleAt(e.DeltaManipulation.Scale.X, e.DeltaManipulation.Scale.Y, e.ManipulationOrigin.X, e.ManipulationOrigin.Y);
            _manipulationMatrix.Translate(e.DeltaManipulation.Translation.X, e.DeltaManipulation.Translation.Y);

            //Width *= e.DeltaManipulation.Scale.X;
            //Height *= e.DeltaManipulation.Scale.Y;

            //_manipulationMatrix.OffsetX = matrixTemp.OffsetX;
            //_manipulationMatrix.OffsetY = matrixTemp.OffsetY;

            RenderTransform = new MatrixTransform(_manipulationMatrix);
        }

        private void UserControl_ManipulationCompleted(object sender, ManipulationCompletedEventArgs e)
        {
            _manipulated = false;
            e.Handled = true;
        }
    }
}
