﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TouchAndTeachWindows.Draw.UserDrawingUI
{
    /// <summary>
    /// Logique d'interaction pour UserDrawingMenu.xaml
    /// </summary>
    public partial class UserDrawingMenu : IDisposable
    {
        public enum UserDrawingSelection
        {
            Postit,
            Transparent,
            Merge,
            Close
        };

        private UserDrawingSelection _userDrawingSelected = UserDrawingSelection.Postit;
        private readonly SolidColorBrush _selectedColor = new SolidColorBrush(Color.FromArgb(255, 197, 242, 255));
        private readonly UserDrawing _userDrawingParent;

        public UserDrawingSelection UserDrawingSelected
        {
            get { return _userDrawingSelected;  }
            set
            {
                _userDrawingSelected = value;
                foreach (Border border in grid.Children.OfType<Border>())
                    border.Background = Brushes.White;

                switch (value)
                {
                    case UserDrawingSelection.Postit:
                        PostitBorder.Background = _selectedColor;
                        _userDrawingParent.SelectedUserDrawingMode = UserDrawing.UserDrawingMode.Postit;
                        break;
                    case UserDrawingSelection.Transparent:
                        TransparentBorder.Background = _selectedColor;
                        _userDrawingParent.SelectedUserDrawingMode = UserDrawing.UserDrawingMode.Transparent;
                        break;
                    case UserDrawingSelection.Merge:
                        MergeBorder.Background = _selectedColor;
                        break;
                    case UserDrawingSelection.Close:
                        CloseBorder.Background = _selectedColor;
                        break;
                }
            }
        }

        public UserDrawingMenu(UserDrawing userDrawingParent)
        {
            _userDrawingParent = userDrawingParent;
            InitializeComponent();
            switch (userDrawingParent.SelectedUserDrawingMode)
            {
                case UserDrawing.UserDrawingMode.Postit:
                    PostitBorder.Background = _selectedColor;
                    break;
                case UserDrawing.UserDrawingMode.Transparent:
                    TransparentBorder.Background = _selectedColor;
                    break;
            }
        }

        private void EventSetter_OnHandler(object sender, InputEventArgs e)
        {
            if (sender.Equals(PostitBorder))
                UserDrawingSelected = UserDrawingSelection.Postit;
            else if (sender.Equals(TransparentBorder))
                UserDrawingSelected = UserDrawingSelection.Transparent;
            else if (sender.Equals(MergeBorder))
                UserDrawingSelected = UserDrawingSelection.Merge;
            else if (sender.Equals(CloseBorder))
                UserDrawingSelected = UserDrawingSelection.Close;
            e.Handled = true;
        }

        private void CloseBorder_OnLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (sender.Equals(CloseBorder))
            {
                _userDrawingParent.Dispose();
                Dispose();
            }
            ReleaseMouseCapture();
            e.Handled = true;
        }

        private void CloseBorder_OnTouchUp(object sender, TouchEventArgs e)
        {
            if (sender.Equals(CloseBorder))
            {
                _userDrawingParent.Dispose();
                Dispose();
            }
            ReleaseTouchCapture(e.TouchDevice);
            e.Handled = true;
        }

        private void MergeBorder_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (sender.Equals(MergeBorder))
            {
                _userDrawingParent.MergeUserDrawing();
                _userDrawingParent.Dispose();
                Dispose();
            }
            ReleaseMouseCapture();
            e.Handled = true;
        }

        private void MergeBorder_OnTouchUp(object sender, TouchEventArgs e)
        {
            if (sender.Equals(MergeBorder))
            {
                _userDrawingParent.MergeUserDrawing();
                _userDrawingParent.Dispose();
                Dispose();
            }
            ReleaseTouchCapture(e.TouchDevice);
            e.Handled = true;
        }

        private void Grid_OnMouseLeave(object sender, InputEventArgs e)
        {
            Dispose();
            ReleaseMouseCapture();
            e.Handled = true;
        }

        private void Grid_OnTouchLeave(object sender, TouchEventArgs e)
        {
            Dispose();
            ReleaseTouchCapture(e.TouchDevice);
            e.Handled = true;
        }

        private void UserDrawingMenu_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Dispose();
            ReleaseMouseCapture();
            e.Handled = true;
        }

        private void UserDrawingMenu_OnTouchUp(object sender, TouchEventArgs e)
        {
            Dispose();
            ReleaseTouchCapture(e.TouchDevice);
            e.Handled = true;
        }

        public void Dispose()
        {
            Panel panel = (Panel)Parent;
            if (panel != null)
            {
                UIElementCollection children = panel.Children;
                if (children != null)
                    children.Remove(this);
            }
        }
    }
}
