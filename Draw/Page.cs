﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Media;
using Newtonsoft.Json;
using TouchAndTeachWindows.Draw.DrawObjects;

namespace TouchAndTeachWindows.Draw
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Page
    {
        public Matrix Transform = Matrix.Identity;
        private readonly List<DrawObject> _notInBitmapDrawObjects; //DrawObject that needs to be drawn on the canvas
        private readonly Dictionary<int, DrawObject> _activeDrawObjects; //store DrawObject that are currently drawn

        

        [JsonProperty("AllDrawObject")]
        private readonly List<DrawObject> _allDrawObjects; //all drawn object

        [JsonProperty("Background")]
        public DrawObject Background { get; set; }

        public bool FirstBackgroundResizeDone { get; private set; }

        public Page()
        {
            _notInBitmapDrawObjects = new List<DrawObject>();
            _activeDrawObjects = new Dictionary<int, DrawObject>();
            _allDrawObjects = new List<DrawObject>();
        }

        public Page(string title) : this()
        {
            Title = title;
        }

        [JsonProperty("Title")]
        public string Title { get; set; }

        public Boolean IsPageEmpty()
        {
            return (_allDrawObjects.Where(obj =>!(obj is Eraser || obj is DrawSelection)).Count() == 0) && (Background == null);
        }

        public List<DrawObject> GetNotInBitmapDrawObjects() { return _notInBitmapDrawObjects; }

        public List<DrawObject> GetAllDrawObjects() { return _allDrawObjects; }

        public void AddPointDown(int id, DrawObject drawObject)
        {
            if (_activeDrawObjects.ContainsKey(id))
                _activeDrawObjects.Remove(id);

            _activeDrawObjects.Add(id, drawObject);

            if (_allDrawObjects.Contains(drawObject)) return;

            _notInBitmapDrawObjects.Add(drawObject);
            _allDrawObjects.Add(drawObject);
        }

        public void AddInactiveObject(DrawObject drawObject)
        {
             _notInBitmapDrawObjects.Add(drawObject);
            _allDrawObjects.Add(drawObject);
        }

        public void RemoveDrawObject(string uuid)
        {
            _notInBitmapDrawObjects.RemoveAll(x => x.Uuid.Equals(uuid));
            _allDrawObjects.RemoveAll(x => x.Uuid.Equals(uuid));
        }

        public void RemoveDrawObject(IEnumerable<string> uuidList)
        {
            HashSet<string> hashSetUuids = new HashSet<string>(uuidList);
            _notInBitmapDrawObjects.RemoveAll(x => hashSetUuids.Contains(x.Uuid));
            _allDrawObjects.RemoveAll(x => hashSetUuids.Contains(x.Uuid));
        }

        public void CancelAllDrawing()
        {
            foreach (DrawObject drawObject in _activeDrawObjects.Values)
            {
                if (_notInBitmapDrawObjects.Contains(drawObject)) _notInBitmapDrawObjects.Remove(drawObject);
                if (_allDrawObjects.Contains(drawObject)) _allDrawObjects.Remove(drawObject);
            }

            _activeDrawObjects.Clear();
        }

        public void AddPointMove(int id, Point pt)
        {
            if (_activeDrawObjects.ContainsKey(id))
            {
                DrawObject drawObject = _activeDrawObjects[id];
                if (drawObject == null) return;

                drawObject.AddPoint(pt, id);

                if (drawObject is Eraser)
                    DrawModel.GetDrawModel().EraseObjects(id, pt, (Eraser)drawObject);
            }
        }

        public void ApplyMatrixToDrawObjects()
        {
            foreach (DrawObject drawObject in _allDrawObjects)
            {
                if (drawObject != null)
                {
                    drawObject.MatrixTransform.Append(Transform);
                }
            }

            if (Background != null)
                Background.MatrixTransform.Append(Transform);

            Transform = Matrix.Identity;
        }

        public void ClearDrawObjects(bool clearAll)
        {
            _notInBitmapDrawObjects.Clear();

            if (clearAll)
                _allDrawObjects.Clear();     
        }

        public DrawObject GetLastDrawObjectAdded()
        {
            DrawObject drawObject = null;
            if (_allDrawObjects != null)
            {
                if (_allDrawObjects.Count > 0)
                {
                    drawObject = _allDrawObjects.Last();
                }
            }
            return drawObject;
        }

        public void ResetUUIDs()
        {
            foreach (DrawObject drawObject in _allDrawObjects)
            {
                drawObject.Uuid = Utils.Utils.GenerateUuid();
            }
        }

        public void ResizeBackgroundToCenter(Size size)
        {
            if (Background == null) return;
            if (Background.MatrixTransform != Matrix.Identity)
            {
                FirstBackgroundResizeDone = true;
                return;
            }
            Rect? bound = Background.GetBounds();
            if (bound == null) return;

            double scale;
            if (size.Width < size.Height)
                scale = size.Width / bound.Value.Width;
            else
                scale = size.Height / bound.Value.Height;
            Background.MatrixTransform.Scale(scale, scale);
            double posX = (size.Width - bound.Value.Width * scale) / 2;
            Background.MatrixTransform.Translate(posX, 0);
            FirstBackgroundResizeDone = true;
        }

        public Rect? GetBoundingBox()
        {
            Rect? bounds = null;
            Point leftTop = new Point(double.MaxValue, double.MaxValue);
            Point rightBottom = new Point();
            if (Background != null)
            {
                if (!FirstBackgroundResizeDone)
                    DrawController.GetDrawController().ResizeBackgroundToCenter(this);
                bounds = Background.GetBounds();
                if (bounds != null)
                {
                    leftTop = new Point(bounds.Value.Left, bounds.Value.Top);
                    rightBottom = new Point(bounds.Value.Right, bounds.Value.Bottom);
                }
            }
            if (_allDrawObjects.Any())
            {
                foreach (DrawObject obj in _allDrawObjects)
                {
                    if (obj != null)
                    {
                        bounds = obj.GetBounds();
                        if (bounds != null)
                        {
                            leftTop = new Point(Math.Min(leftTop.X, bounds.Value.Left), Math.Min(leftTop.Y, bounds.Value.Top));
                            rightBottom = new Point(Math.Max(rightBottom.X, bounds.Value.Right), Math.Max(rightBottom.Y, bounds.Value.Bottom));
                        }
                    }
                }
            }
            if (bounds != null)
            {
                if (Math.Abs(leftTop.X - double.MaxValue) > double.Epsilon)
                    return new Rect(leftTop, rightBottom);
            }
            return null;
        }

        [OnDeserialized]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            foreach (DrawObject drawObject in _allDrawObjects)
                _notInBitmapDrawObjects.Add(drawObject);
        }
    }
}
