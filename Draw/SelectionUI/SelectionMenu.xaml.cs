﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TouchAndTeachWindows.Draw.SelectionUI
{

    public enum SelectionMenuAction { None, Merge, AvailableDraw, Close };

    /// <summary>
    /// Interaction logic for SelectionMenu.xaml
    /// </summary>
    public partial class SelectionMenu : UserControl/*, IDisposable*/
    {

        private readonly SolidColorBrush _selectedColor = new SolidColorBrush(Color.FromArgb(255, 197, 242, 255));
        public event Action<SelectionMenu, SelectionMenuAction> ActionActivated = (menu, action) => { };


        public SelectionMenuAction ActionSelected
        {
            get { return (SelectionMenuAction)GetValue(ActionSelectedProperty); }
            set { SetValue(ActionSelectedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ActionSelected.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ActionSelectedProperty = DependencyProperty.Register("ActionSelected", typeof(SelectionMenuAction), typeof(SelectionMenu), new PropertyMetadata(SelectionMenuAction.None));

        public SelectionMenu()
        {
            InitializeComponent();
        }

        private void EventSetter_OnHandler(object sender, InputEventArgs e)
        {
            //Reinit background color
            foreach (Border border in grid.Children.OfType<Border>())
                border.Background = Brushes.White;

            if (sender.Equals(MergeBorder)) 
            {
                MergeBorder.Background = _selectedColor;
                ActionSelected = SelectionMenuAction.Merge; //Set back color
            }
            else if (sender.Equals(CloseBorder))
            {
                CloseBorder.Background = _selectedColor;
                ActionSelected = SelectionMenuAction.Close; //Set back color
            }
            else if (sender.Equals(AvailableDrawBorder))
            {
                AvailableDrawBorder.Background = _selectedColor;  //Set back color
                ActionSelected = SelectionMenuAction.AvailableDraw;
            }

            //CaptureInput(e);

            e.Handled = true;
        }

        private void Ellipse_DeviceEnter(object sender, InputEventArgs e)
        {
            ActionSelected = SelectionMenuAction.None;
            //CaptureInput(e);
        }

        private void CaptureInput(InputEventArgs e)
        {
            if (e.Device is TouchDevice)
                CaptureTouch((TouchDevice)e.Device);
            else if (e.Device is MouseDevice)
                CaptureMouse();
            else if (e.Device is StylusDevice)
                CaptureStylus();
        }

        //public void Dispose()
        //{
        //    Panel parent = Parent as Panel;
        //    parent.Children.Remove(this);

        //    ReleaseAllTouchCaptures();
        //    ReleaseMouseCapture();
        //    ReleaseStylusCapture();
        //}

        private void This_TouchUp(object sender, TouchEventArgs e)
        {
            //Dispose();
            ActionActivated(this, ActionSelected);
            e.Handled = true;
        }

        private void This_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            //Dispose();
            ActionActivated(this, ActionSelected);
            e.Handled = true;
        }
    }
}
