﻿using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Media;
using System.Xml.Linq;
using Newtonsoft.Json;
using System.Text;
using System.Globalization;

namespace TouchAndTeachWindows.Draw.DrawObjects
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Line : DrawObject
    {
        private static readonly Point InvalidPoint = new Point(-1, -1);
        private GeometryDrawing _shape;

        [JsonProperty("Start")]
        private Point _start = InvalidPoint;

        [JsonProperty("Stop")]
        private Point _stop = InvalidPoint;

        private bool _startPoint = true;
        private PathGeometry _path;

        [JsonProperty("Painter")]
        public Painter Painter { get; set; }

        public Line(Painter painter) : this()
        {
            Uuid = Utils.Utils.GenerateUuid();
            Painter = painter;
        }

        public Line() : base()
        {
            ObjectType = typeof (Line).Name;
        }

        public override void AddToDrawingContext(DrawingContext drawingContext)
        {
            drawingContext.PushTransform(new MatrixTransform(MatrixTransform));
            drawingContext.DrawDrawing(_shape);
        }

        public override void AddPoint(Point pt, int? id = null)
        {
            if (_startPoint)
            {
                _startPoint = false;
                _start = pt;
                InitPath();
            }
            else
            {
                _stop = pt;
                WritePath();
            }
        }

        public override Rect? GetBounds()
        {
            if (_path == null) return null;

            Rect bound = _path.Bounds;
            if (!bound.IsEmpty)
                bound.Inflate(Painter.StrokeWidth / 2, Painter.StrokeWidth / 2);
            bound.Transform(MatrixTransform);
            return bound;
        }

        private void InitPath()
        {
            if (_shape != null) return;

            _path = new PathGeometry();
            _shape = new GeometryDrawing
            {
                Pen =
                    new Pen(Painter.Color, Painter.StrokeWidth)
                    {
                        DashCap = PenLineCap.Round,
                        EndLineCap = PenLineCap.Round,
                        LineJoin = PenLineJoin.Round,
                        StartLineCap = PenLineCap.Round
                    },
                Geometry = _path
            };
        }

        private void WritePath()
        {
            GeometryGroup geometryGroup = new GeometryGroup();
            geometryGroup.Children.Add(new LineGeometry(_start, _stop));
            _path.Clear();
            _path.AddGeometry(geometryGroup);
        }

        public override Geometry GetGeometry()
        {
             return new LineGeometry(_start, _stop, new MatrixTransform(MatrixTransform));
        }

        public override XElement GetSVG(XNamespace xmlns, Matrix transform)
        {
            Matrix svgMatrix = MatrixTransform.Clone();
            svgMatrix.Append(transform);

            return new XElement(xmlns + "line",
                new XAttribute("stroke", string.Format(CultureInfo.InvariantCulture, "rgb({0},{1},{2})", Painter.Color.Color.R, Painter.Color.Color.G, Painter.Color.Color.B)),
                new XAttribute("stroke-width", Painter.StrokeWidth.ToString()),
                new XAttribute("stroke-linecap", "round"),
                new XAttribute("x1", _start.X.ToString(CultureInfo.InvariantCulture)),
                new XAttribute("y1", _start.Y.ToString(CultureInfo.InvariantCulture)),
                new XAttribute("x2", _stop.X.ToString(CultureInfo.InvariantCulture)),
                new XAttribute("y2", _stop.Y.ToString(CultureInfo.InvariantCulture)),
                new XAttribute("transform",
                    string.Format(CultureInfo.InvariantCulture,
                        "matrix({0},{1},{2},{3},{4},{5})",
                        svgMatrix.M11,
                        svgMatrix.M12,
                        svgMatrix.M21,
                        svgMatrix.M22,
                        svgMatrix.OffsetX,
                        svgMatrix.OffsetY))
                );
        }

        [OnDeserialized]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            InitPath();
            WritePath();
        }
    }
}
