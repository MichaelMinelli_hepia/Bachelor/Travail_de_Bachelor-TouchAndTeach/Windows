﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace TouchAndTeachWindows.Draw.DrawObjects
{
    public class Eraser : DrawObject
    {

        private GeometryDrawing _shape;
        private EllipseGeometry _ellipse;

        public Eraser() : base()
        {
            Uuid = Utils.Utils.GenerateUuid();
            ObjectType = typeof(Eraser).Name;

            _ellipse = new EllipseGeometry(new Point(), 20, 20);
            _shape = new GeometryDrawing
            {
                Pen = new Pen(Brushes.Red, 1),
                Geometry = _ellipse
            };
        }


        public override void AddPoint(Point pt, int? id = default(int?))
        {
            _ellipse.Center = pt;
        }

        public override void AddToDrawingContext(DrawingContext drawingContext)
        {
            drawingContext.DrawDrawing(_shape);
        }


        /// <summary>
        /// Returns a list of Tuple containing the DrawObject to erase as Item1, and if the DrawObject is an Highlighter then
        /// only the figure with the index as item2 must be erased. If the index is -1 then all the highlighter must be erased.
        /// </summary>
        /// <param name="allDrawObjects">All the DrawObject to check</param>
        /// <returns></returns>
        public IEnumerable<Tuple<DrawObject, int>> GetAllDrawObjectToErase(IEnumerable<DrawObject> allDrawObjects)
        {
            Rect? bound = GetBounds();
            if (bound == null) yield break;

            foreach (DrawObject obj in allDrawObjects)
            {
                Rect? objBound = obj.GetBounds();
                if (objBound == null) continue;
                if (!bound.Value.IntersectsWith(objBound.Value)) continue;
                if (bound.Value.Contains(objBound.Value))
                    yield return new Tuple<DrawObject, int>(obj, -1);

                if (obj is Highlighter)
                {
                    foreach (var figure in ((Highlighter)obj).GetFiguresGeometry())
                        if (_ellipse.FillContainsWithDetail(figure.Value, 1, ToleranceType.Absolute) != IntersectionDetail.Empty)
                            yield return new Tuple<DrawObject, int>(obj, figure.Key);

                }
                else if (_ellipse.FillContainsWithDetail(obj.GetGeometry(), 1, ToleranceType.Absolute) != IntersectionDetail.Empty)
                    yield return new Tuple<DrawObject, int>(obj, -1);
            }
        }

        public override Rect? GetBounds()
        {
            return _ellipse.Bounds;
        }

        public override Geometry GetGeometry()
        {
            return _ellipse;
        }
    }
}
