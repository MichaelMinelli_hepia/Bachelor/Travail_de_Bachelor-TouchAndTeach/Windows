﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TouchAndTeachWindows.Utils;

namespace TouchAndTeachWindows.Draw.DrawObjects
{
    [JsonObject(MemberSerialization.OptIn)]
    public abstract class DrawObject
    {
        [JsonProperty("ObjectType")]
        protected string ObjectType { get; set; }

        [JsonProperty("MatrixTransform")]
        public Matrix MatrixTransform = Matrix.Identity;

        [JsonProperty("Uuid")]
        public string Uuid { get; set; }

        public abstract Geometry GetGeometry();

        public abstract void AddToDrawingContext(DrawingContext drawingContext);

        public virtual void AddPoint(Point pt, int? id = null) { }

        public abstract Rect? GetBounds();

        // http://www.w3.org/TR/SVG/Overview.html
        public virtual XElement GetSVG(XNamespace xmlns, Matrix transform) { return null; }

        public class DrawObjectConverter : JsonCreationConverter<DrawObject>
        {
            protected override DrawObject Create(Type objectType, JObject jObject)
            {
                string objType = (string)jObject["ObjectType"];
                if (objType.Equals(typeof(Stroke).Name))
                    return new Stroke();
                if (objType.Equals(typeof(Rectangle).Name))
                    return new Rectangle();
                if (objType.Equals(typeof(Ellipse).Name))
                    return new Ellipse();
                if (objType.Equals(typeof(DrawImage).Name))
                    return new DrawImage();
                if (objType.Equals(typeof(Line).Name))
                    return new Line();
                if (objType.Equals(typeof(DrawPDF).Name))
                    return new DrawPDF();
                if (objType.Equals(typeof(Highlighter).Name))
                    return new Highlighter();
                return null;
            }
        }
    }
}
