﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows;

namespace TouchAndTeachWindows.Draw.DrawObjects
{
    public class DrawSelection :DrawObject
    {
        public Painter Painter { get; set; }

        private readonly List<Point> _pointList;
        private GeometryDrawing _shape;
        private Point _lastPoint, _endPoint;
        private QuadraticBezierSegment _bezierSegment;
        private PathGeometry _path;
        private PathFigure _figure;

        public DrawSelection()
            : base()
        {
            ObjectType = typeof(DrawSelection).Name;
            Uuid = Utils.Utils.GenerateUuid();
            _pointList = new List<Point>();
        }

        public override void AddToDrawingContext(DrawingContext drawingContext)
        {
            drawingContext.DrawDrawing(_shape);
        }

        public override void AddPoint(Point pt, int? id = null)
        {
            PointToPath(pt);
            _pointList.Add(pt);
        }

        public void PointToPath(Point pt)
        {
            if (_shape == null)
            {
                _path = new PathGeometry();
                _figure = new PathFigure { StartPoint = pt };
                _lastPoint = new Point(pt.X, pt.Y);
                _endPoint = new Point();
                _shape = new GeometryDrawing
                {
                    Pen =
                        new Pen(Brushes.DarkGray ,1)
                        {
                            DashCap = PenLineCap.Round,
                            EndLineCap = PenLineCap.Round,
                            LineJoin = PenLineJoin.Round,
                            StartLineCap = PenLineCap.Round,
                            DashStyle = DashStyles.Dash
                        },
                };
                _path.Figures.Add(_figure);
                _shape.Geometry = _path;

                //_figure.Segments.Add(new PolyLineSegment(new List<Point>(), true));
                //((PolyLineSegment)_figure.Segments[0]).Points.Add(pt);
            }
            else
            {
                _endPoint.X = (pt.X + _lastPoint.X) / 2;
                _endPoint.Y = (pt.Y + _lastPoint.Y) / 2;
                _bezierSegment = new QuadraticBezierSegment(_lastPoint, _endPoint, true);
                _figure.Segments.Add(_bezierSegment);
                _lastPoint = pt;
                //((PolyLineSegment)_figure.Segments.Last()).Points.Add(pt);
            }
        }


        public override System.Windows.Rect? GetBounds()
        {
            if (_path == null) return null;

            Rect bound = _path.Bounds;
            bound.Transform(MatrixTransform);
            return bound;
        }

        public override Geometry GetGeometry()
        {
            return null;
        }

        public bool IsInSelectionPath(Geometry geometry)
        {
            if (geometry == null) return false;
            return _path.FillContains(geometry, 1, ToleranceType.Absolute);
        }


        public IEnumerable<Tuple<DrawObject, List<int>>> GetAllInSelectionPath(IEnumerable<DrawObject> drawObjects)
        {
            Rect? bound = GetBounds();
            if (bound == null) yield break;
            
            foreach (DrawObject obj in drawObjects)
            {
                Rect? objBound = obj.GetBounds();
                if (objBound == null) continue;

                if (obj is Highlighter)
                {
                    List<int> figIds = 
                        ((Highlighter)obj).GetFiguresGeometry().
                        Where(f => _path.FillContains(f.Value, 1, ToleranceType.Absolute)).
                        Select(f => f.Key).
                        ToList();

                    if (figIds.Count > 0)
                        yield return new Tuple<DrawObject, List<int>>(obj, figIds);
                    continue;
                }

                if (!bound.Value.Contains(objBound.Value)) continue;
                if (!IsInSelectionPath(obj.GetGeometry())) continue;
                yield return new Tuple<DrawObject, List<int>>(obj, null);
            }
        }
    }
}
