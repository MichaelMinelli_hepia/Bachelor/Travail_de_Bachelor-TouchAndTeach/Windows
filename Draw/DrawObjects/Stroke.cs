﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Media;
using Newtonsoft.Json;
using System.Xml;
using System.Text;
using System.Xml.Linq;
using System.Globalization;

namespace TouchAndTeachWindows.Draw.DrawObjects
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Stroke : DrawObject
    {
        [JsonProperty("PointList")]
        private readonly List<Point> _pointList;
        private GeometryDrawing _shape;
        private PathGeometry _path;
        private PathFigure _figure;

        [JsonProperty("Painter")]
        public Painter Painter { get; set; }

        [JsonProperty("StrokeType")]
        private string type = "LowDensity"; //Utily : Only for iPad sharing (for performance if the stroke have been done by stylus)

        private Point _lastPoint, _endPoint;
        private QuadraticBezierSegment _bezierSegment;

        public Stroke(Painter painter) : this()
        {
            Uuid = Utils.Utils.GenerateUuid();
            Painter = painter;
            _pointList = new List<Point>();
        }

        public Stroke() : base()
        {
            ObjectType = typeof(Stroke).Name;
        }

        public override void AddToDrawingContext(DrawingContext drawingContext)
        {
            drawingContext.PushTransform(new MatrixTransform(MatrixTransform));
            drawingContext.DrawDrawing(_shape);
        }

        public override void AddPoint(Point pt, int? id = null)
        {
            PointToPath(pt);
            _pointList.Add(pt);
        }

        public void PointToPath(Point pt)
        {
            if (_shape == null)
            {
                _path = new PathGeometry();
                _figure = new PathFigure { StartPoint = pt };
                _lastPoint = new Point(pt.X, pt.Y);
                _endPoint = new Point();
                _shape = new GeometryDrawing
                {
                    Pen =
                        new Pen(Painter.Color, Painter.StrokeWidth)
                        {
                            DashCap = PenLineCap.Round,
                            EndLineCap = PenLineCap.Round,
                            LineJoin = PenLineJoin.Round,
                            StartLineCap = PenLineCap.Round
                        }
                };
                _path.Figures.Add(_figure);
                _shape.Geometry = _path;
            }
            else
            {
                _endPoint.X = (pt.X + _lastPoint.X) / 2;
                _endPoint.Y = (pt.Y + _lastPoint.Y) / 2;
                _bezierSegment = new QuadraticBezierSegment(_lastPoint, _endPoint, true);
                _figure.Segments.Add(_bezierSegment);
                _lastPoint = pt;
            }
        }

        public override Rect? GetBounds()
        {
            if (_path == null) return null;

            Rect bound = _path.Bounds;
            if (!bound.IsEmpty)
                bound.Inflate(Painter.StrokeWidth / 2, Painter.StrokeWidth / 2);
            bound.Transform(MatrixTransform);
            return bound;
            //return path.Bounds;
        }

        public override Geometry GetGeometry()
        {
            return new PathGeometry(new List<PathFigure> { _figure }, FillRule.EvenOdd, new MatrixTransform(MatrixTransform));
        }

        public override XElement GetSVG(XNamespace xmlns, Matrix transform)
        {
            StringBuilder path = new StringBuilder(string.Format(CultureInfo.InvariantCulture, "M {0} {1} ", _pointList[0].X, _pointList[0].Y));
            for (int i = 1; i < _pointList.Count; i++)
                path.AppendFormat(CultureInfo.InvariantCulture,
                    "Q {0} {1} {2} {3} ",
                    _pointList[i - 1].X,
                    _pointList[i - 1].Y,
                    (_pointList[i].X + _pointList[i - 1].X) / 2,
                    (_pointList[i].Y + _pointList[i - 1].Y) / 2);

            Matrix svgMatrix = MatrixTransform.Clone();
            svgMatrix.Append(transform);

            return new XElement(xmlns + "path",
                new XAttribute("fill", "none"),
                new XAttribute("stroke", string.Format(CultureInfo.InvariantCulture, "rgb({0},{1},{2})", Painter.Color.Color.R, Painter.Color.Color.G, Painter.Color.Color.B)),
                new XAttribute("stroke-width", Painter.StrokeWidth.ToString()),
                new XAttribute("stroke-linecap", "round"),
                new XAttribute("transform",
                    string.Format(CultureInfo.InvariantCulture,
                        "matrix({0},{1},{2},{3},{4},{5})",
                        svgMatrix.M11,
                        svgMatrix.M12,
                        svgMatrix.M21,
                        svgMatrix.M22,
                        svgMatrix.OffsetX,
                        svgMatrix.OffsetY)),
                new XAttribute("d", path.ToString())
                );
        }

        [OnDeserialized]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            foreach (Point pt in _pointList)
                PointToPath(pt);
        }
    }
}
