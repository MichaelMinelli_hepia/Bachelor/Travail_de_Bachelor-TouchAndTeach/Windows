﻿using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Media;
using Newtonsoft.Json;

namespace TouchAndTeachWindows.Draw.DrawObjects
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Ellipse : DrawObject
    {
        private static readonly Point InvalidPoint = new Point(-1, -1);
        private GeometryDrawing _shape;

        [JsonProperty("Start")]
        private Point _start = InvalidPoint;

        [JsonProperty("Stop")]
        private Point _stop = InvalidPoint;

        private bool _startPoint = true;
        private PathGeometry _path;

        [JsonProperty("Painter")]
        public Painter Painter { get; protected set; }

        public Ellipse(Painter painter) : this()
        {
            Uuid = Utils.Utils.GenerateUuid();
            Painter = painter;
        }

        public Ellipse() : base()
        {
            ObjectType = typeof (Ellipse).Name;
        }

        public override void AddToDrawingContext(DrawingContext drawingContext)
        {
            drawingContext.PushTransform(new MatrixTransform(MatrixTransform));
            drawingContext.DrawDrawing(_shape);
        }

        public override void AddPoint(Point pt, int? id = null)
        {
            if (_startPoint)
            {
                _startPoint = false;
                _start = pt;
                InitPath();
            }
            else
            {
                _stop = pt;
                WritePath();
            }
        }

        public override Rect? GetBounds()
        {
            if (_path == null) return null;

            Rect bound = _path.Bounds;
            if (!bound.IsEmpty)
                bound.Inflate(Painter.StrokeWidth / 2, Painter.StrokeWidth / 2);
            bound.Transform(MatrixTransform);
            return bound;
        }

        private void InitPath()
        {
            if (_shape != null) return;

            _path = new PathGeometry();
            _shape = new GeometryDrawing
            {
                Pen =
                    new Pen(Painter.Color, Painter.StrokeWidth)
                    {
                        DashCap = PenLineCap.Round,
                        EndLineCap = PenLineCap.Round,
                        LineJoin = PenLineJoin.Round,
                        StartLineCap = PenLineCap.Round
                    },
                Geometry = _path
            };
        }

        private void WritePath()
        {
            GeometryGroup geometryGroup = new GeometryGroup();
            geometryGroup.Children.Add(new EllipseGeometry(new Rect(_start, _stop)));
            _path.Clear();
            _path.AddGeometry(geometryGroup);
        }

        public override Geometry GetGeometry()
        {
            return _path.Clone();
        }

        [OnDeserialized]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            InitPath();
            WritePath();
        }
    }
}
