﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using Newtonsoft.Json;
using Point = System.Windows.Point;
using Size = System.Windows.Size;
using System.Globalization;

namespace TouchAndTeachWindows.Draw.DrawObjects
{
    [JsonObject(MemberSerialization.OptIn)]
    class DrawImage : DrawObject
    {
        public enum ImageType
        {
            Jpeg,
            Png
        };
        private BitmapImage _image;
        [JsonProperty("ImageType")]
        private ImageType _imageType;
        [JsonProperty("ImageArray")]
        private readonly byte[] _imageArray;
        private Rect _bounds;

        public DrawImage()
        {
            ObjectType = typeof (DrawImage).Name;
        }

        public DrawImage(BitmapSource bitmapSource) : this()
        {
            Uuid = Utils.Utils.GenerateUuid();

            PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmapSource));
            MemoryStream ms = new MemoryStream();

            encoder.Save(ms);
            _imageArray = ms.ToArray();

            BitmapImage bImg = new BitmapImage();

            bImg.BeginInit();
            bImg.StreamSource = new MemoryStream(_imageArray);
            bImg.EndInit();

            ms.Close();

            _image = bImg;
            _imageType = ImageType.Jpeg;
            _bounds = new Rect(new Size(_image.PixelWidth, _image.PixelHeight));
        }

        public DrawImage(BitmapImage bitmapImage) : this()
        {
            Uuid = Utils.Utils.GenerateUuid();
            _image = bitmapImage;
            _imageType = ImageType.Png;
            _bounds = new Rect(new Size(_image.PixelWidth, _image.PixelHeight));

            //save the image as byte array for serialization
            PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(_image));
            using(MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                _imageArray = ms.ToArray();
            }
        }

        public override void AddToDrawingContext(DrawingContext drawingContext)
        {
            drawingContext.PushTransform(new MatrixTransform(MatrixTransform));
            drawingContext.DrawImage(_image, _bounds);
        }

        public override void AddPoint(Point pt, int? id = null)
        {
        }

        public override Rect? GetBounds()
        {
            Rect bound = new Rect(_bounds.Size);
            bound.Transform(MatrixTransform);
            return bound;
        }

        public override Geometry GetGeometry()
        {
            return new RectangleGeometry(new Rect(_bounds.Size), 0, 0, new MatrixTransform(MatrixTransform));
        }

        public override XElement GetSVG(XNamespace xmlns, Matrix transform)
        {
            XNamespace xlink = "http://www.w3.org/1999/xlink";
            string imgBase64 = Convert.ToBase64String(_imageArray);

            Matrix svgMatrix = MatrixTransform.Clone();
            svgMatrix.Append(transform);

            return new XElement(xmlns + "image",
                new XAttribute(xlink + "href", "data:image/png;base64," + imgBase64),
                new XAttribute("height", _image.PixelHeight),
                new XAttribute("width", _image.PixelWidth),
                new XAttribute("transform",
                    string.Format(CultureInfo.InvariantCulture, "matrix({0},{1},{2},{3},{4},{5})",
                        svgMatrix.M11,
                        svgMatrix.M12,
                        svgMatrix.M21,
                        svgMatrix.M22,
                        svgMatrix.OffsetX,
                        svgMatrix.OffsetY)));
        }

        [OnDeserialized]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            using (MemoryStream ms = new MemoryStream(_imageArray))
            {
                ms.Position = 0;
                _image = new BitmapImage();
                _image.BeginInit();
                _image.CacheOption = BitmapCacheOption.OnLoad;
                _image.StreamSource = ms;
                _image.EndInit();
            }
            _bounds = new Rect(new Size(_image.PixelWidth, _image.PixelHeight));
        }
    }
}
