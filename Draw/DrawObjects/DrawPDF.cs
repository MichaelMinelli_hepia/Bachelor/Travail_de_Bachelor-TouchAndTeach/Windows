﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using DocToWPF;
using Newtonsoft.Json;
using System.Threading;
using System.Windows.Media.Imaging;
using System.Diagnostics;
using System.IO;
using System.Xml.Linq;
using System.Globalization;
//using Cairo;

namespace TouchAndTeachWindows.Draw.DrawObjects
{
    [JsonObject(MemberSerialization.OptIn)]
    public class DrawPDF : DrawObject
    {
        private DocToWPF.PDFViewer _pdfViewer;
        private SemaphoreSlim _waitEventMutex = new SemaphoreSlim(1);

        private BitmapSource _bitmapSource;

        private Size _initialSize;

        private double _scale = 0;
        

        [JsonProperty("FileName")]
        public string FileName
        {
            get { return System.IO.Path.GetFileName(_pdfViewer.FileName); }
        }

        [JsonProperty("Page")]
        private int _page;
        public int Page
        {
            get { return _page; }
            private set { if (value < _pdfViewer.NumberOfPages)
                    _page = value;
            }
        }

        public DrawPDF() : base()
        {
            ObjectType = typeof (DrawPDF).Name;
            Uuid = Utils.Utils.GenerateUuid();
        }


        public DrawPDF(DocToWPF.PDFViewer pdfViewer, int page = 0) : this()
        {
            _pdfViewer = pdfViewer;

            Page = page;

            Size size = DrawController.GetDrawController().GetAreaSize();
            Size pageSize = _pdfViewer.PageSizeList[page];

            if (size.Width / pageSize.Width > size.Height / pageSize.Height)
                _initialSize = new Size((int)(pageSize.Width * (size.Height / pageSize.Height)), (int)size.Height);
            else
                _initialSize = new Size((int)size.Width, (int)(pageSize.Height * (size.Width / pageSize.Width)));
        }


        public override System.Windows.Media.Geometry GetGeometry()
        {
            return new RectangleGeometry(new Rect(_initialSize), 0, 0, new MatrixTransform(MatrixTransform));
        }


        public override void AddToDrawingContext(System.Windows.Media.DrawingContext drawingContext)
        {
            if (MatrixTransform.M11 != _scale || _bitmapSource == null)
            {
                _scale = MatrixTransform.M11;
                _bitmapSource = _pdfViewer.ShowPdf(page: Page, width: (int)(_initialSize.Width * _scale), height: (int)(_initialSize.Height * _scale));
            }

            drawingContext.PushTransform(new MatrixTransform(MatrixTransform));
            drawingContext.DrawImage(_bitmapSource, new Rect(_initialSize));
        }


        public override XElement GetSVG(XNamespace xmlns, Matrix transform)
        {
            string tempFilePath = System.IO.Path.GetTempPath() + System.IO.Path.GetRandomFileName();
            FileStream stream = File.Create(tempFilePath);
            stream.Close();

            Process p = new Process();

            
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.FileName = @"./Dependencies/poppler/exec/pdftocairo.exe";
            p.StartInfo.Arguments = string.Format(CultureInfo.InvariantCulture, "-svg -f {0} -l {0} \"{1}\" \"{2}\"", Page + 1, _pdfViewer.FileName, tempFilePath);
            
            p.Start();

            //string output = p.StandardOutput.ReadToEnd();
            p.WaitForExit();

            StreamReader reader = new StreamReader(tempFilePath);
            string output = reader.ReadToEnd();
            reader.Close();
            File.Delete(tempFilePath);

            XDocument document = XDocument.Parse(output);
            XElement element = document.Root;
            if (element == null) return null;
            element.Name = xmlns + "g";

            string widthString = element.Attribute("width").Value;
            widthString = widthString.Replace("pt", "");
            double width = double.Parse(widthString, CultureInfo.InvariantCulture);

            string heightString = element.Attribute("height").Value;
            heightString = heightString.Replace("pt", "");
            double height = double.Parse(heightString, CultureInfo.InvariantCulture);

            Matrix svgMatrix = MatrixTransform.Clone();
            svgMatrix.Append(transform);

            svgMatrix.ScalePrepend(_initialSize.Width / width, _initialSize.Height / height);

            element.Add(new XAttribute("transform",
                    string.Format(CultureInfo.InvariantCulture, "matrix({0},{1},{2},{3},{4},{5})",
                        svgMatrix.M11,
                        svgMatrix.M12,
                        svgMatrix.M21,
                        svgMatrix.M22,
                        svgMatrix.OffsetX,
                        svgMatrix.OffsetY)));
            return element;
        }


        public override System.Windows.Rect? GetBounds()
        {
            Rect bound = new Rect(_initialSize);
            bound.Transform(MatrixTransform);
            return bound;
        }
    }
}
