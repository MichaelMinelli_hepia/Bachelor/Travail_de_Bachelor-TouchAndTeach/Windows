﻿using System.Windows;
using System.Windows.Controls;
using TouchAndTeachWindows.Comm;
using TouchAndTeachWindows.Draw;

namespace TouchAndTeachWindows.Menu
{
    /// <summary>
    /// Logique d'interaction pour NotificationList.xaml
    /// </summary>
    public partial class NotificationList : UserControl
    {
        public NotificationList()
        {
            InitializeComponent();
            MessageManager messageManager = MessageManager.GetMessageManager();
            NotificationListBox.ItemsSource = messageManager.GetMessageCollection();
        }

        private void NotificationElement_Pressed(object sender, RoutedEventArgs e)
        {
        }

        private void ScrollViewer_ManipulationBoundaryFeedback(object sender, System.Windows.Input.ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}
