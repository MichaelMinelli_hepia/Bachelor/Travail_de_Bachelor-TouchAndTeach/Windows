﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TouchAndTeachWindows.Draw;
using DrawType = TouchAndTeachWindows.Draw.DrawModel.DrawObjectType;

namespace TouchAndTeachWindows.Menu.MenuPen
{
    /// <summary>
    /// Interaction logic for PenType.xaml
    /// </summary>
    public partial class PenType : UserControl
    {
        public DrawType PenTypeSelected
        {
            get { return (DrawType)GetValue(PenTypeSelectedProperty); }
            set { SetValue(PenTypeSelectedProperty, value); }
        }

        public static readonly DependencyProperty PenTypeSelectedProperty =
            DependencyProperty.Register("PenTypeSelected", typeof(DrawType), typeof(PenType), 
                new PropertyMetadata(DrawController.GetDrawController().GetDrawObjectType(), 
                    (obj, arg) => DrawController.GetDrawController().SetDrawObjectStyle((DrawType)arg.NewValue)));


        DrawController _controller;


        public PenType()
        {
            InitializeComponent();
            _controller = DrawController.GetDrawController();
            SetValue(PenTypeSelectedProperty, _controller.GetDrawObjectType());

            Painter painter = _controller.GetPainter();
            PenBorder.DataContext = painter;
            RectangleBorder.DataContext = painter;
            LineBorder.DataContext = painter;

            HighlighterBorder.DataContext = _controller.GetHighlighterPainter();
        }


        private void EventSetter_OnHandler(object sender, InputEventArgs e)
        {
            if (sender.Equals(PenBorder))
                PenTypeSelected = DrawType.Stroke;
                /*
            else if (sender.Equals(EllipseBorder))
                PenTypeSelected = PenTypeEnum.Ellipse;
            */
            else if (sender.Equals(HighlighterBorder))
                PenTypeSelected = DrawType.Highlighter;
            else if (sender.Equals(RectangleBorder))
                PenTypeSelected = DrawType.Rectangle;
            else if (sender.Equals(LineBorder))
                PenTypeSelected = DrawType.Line;
            e.Handled = true;
        }

        private void This_TouchDown(object sender, TouchEventArgs e)
        {
            if (Parent != null)
                ((Panel)Parent).Children.Remove(this);
        }

        private void This_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (Parent != null)
                ((Panel)Parent).Children.Remove(this);
        }

    }
}
