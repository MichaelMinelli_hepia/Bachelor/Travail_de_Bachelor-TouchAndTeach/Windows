﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TouchAndTeachWindows.Draw;

namespace TouchAndTeachWindows.Menu.MenuPen
{
    /// <summary>
    /// Interaction logic for PenSize.xaml
    /// </summary>
    public partial class PenSize : UserControl
    {
        public StrokeWidthEnum StrokeWidth
        {
            get { return (StrokeWidthEnum)GetValue(StrokeWidthProperty); }
            set { SetValue(StrokeWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for StrokeWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StrokeWidthProperty =
            DependencyProperty.Register("StrokeWidth", typeof(StrokeWidthEnum), typeof(PenSize), 
                new PropertyMetadata(StrokeWidthEnum.Thin, 
                (obj, arg) => { DrawController.GetDrawController().GetCurrentlyUsedPainter().StrokeWidth = (int)arg.NewValue; }));

        public PenSize()
        {
            InitializeComponent();
            SetValue(PenSize.StrokeWidthProperty, (StrokeWidthEnum)DrawController.GetDrawController().GetCurrentlyUsedPainter().StrokeWidth);
            PenSizeListView.SelectionChanged += ListView_SelectionChanged;
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Parent != null)
                ((Panel)Parent).Children.Remove(this);
        }
    }
}
