﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using TouchAndTeachWindows.Comm;
using TouchAndTeachWindows.Draw;
using TouchAndTeachWindows.Draw.DrawObjects;
using Page = TouchAndTeachWindows.Draw.Page;
using Point = System.Windows.Point;

namespace TouchAndTeachWindows.Menu
{
    /// <summary>
    /// Logique d'interaction pour NotificationElement.xaml
    /// </summary>
    public partial class NotificationElement : UserControl
    {
        #region Events Declaration
        public static readonly RoutedEvent PressedEvent = EventManager.RegisterRoutedEvent("NotificationElementPressed",
            RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(MenuButton));

        public event RoutedEventHandler Pressed
        {
            add { AddHandler(PressedEvent, value); }
            remove { RemoveHandler(PressedEvent, value); }
        }

        private readonly RoutedEventArgs _pressedArgs = new RoutedEventArgs(PressedEvent);
        #endregion

        private readonly DrawController _drawController = DrawController.GetDrawController();
        private readonly MessageManager _messageManager = MessageManager.GetMessageManager();

        public NotificationElement()
        {
            InitializeComponent();
        }

        private void UserControl_TouchUp(object sender, TouchEventArgs e)
        {
            RaiseEvent(_pressedArgs);
            e.Handled = true;
        }

        private void UserControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            RaiseEvent(_pressedArgs);
            e.Handled = true;
        }

        private void UserControl_TouchDown(object sender, TouchEventArgs e)
        {
            e.Handled = true;
        }

        private void Accept_Pressed(object sender, RoutedEventArgs e)
        {
            if (!(DataContext is Message)) return;
            Message message = (Message)DataContext;
            _drawController.AddUserDrawing(message.Page);
            _messageManager.RemoveMessage(message);
        }

        private void Dismiss_Pressed(object sender, RoutedEventArgs e)
        {
            if (!(DataContext is Message)) return;
            Message message = (Message)DataContext;
            _messageManager.RemoveMessage(message);
        }
    }

    class NotificationConverter : IValueConverter
    {
        private const int NotificationElementSize = 80;
        private double _scale;
        private RenderTargetBitmap _bitmap;

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is Page)) return value;

            Page page = (Page) value;
            if (page.GetAllDrawObjects().Count <= 0) return value;

            Rect? bound = page.GetBoundingBox();

            Point pos = new Point();
            if (bound != null)
            {
                if (bound.Value.Width < bound.Value.Height)
                    _scale = NotificationElementSize / bound.Value.Height;
                else
                    _scale = NotificationElementSize / bound.Value.Width;
                pos = new Point((NotificationElementSize / _scale - bound.Value.Width) / 2 - bound.Value.Left,
                    (NotificationElementSize / _scale - bound.Value.Height) / 2 - bound.Value.Top);
            }

            _bitmap = new RenderTargetBitmap(NotificationElementSize, NotificationElementSize, 96, 96, PixelFormats.Pbgra32);
            DrawingVisual drawingVisual = new DrawingVisual();

            using (DrawingContext drawingContext = drawingVisual.RenderOpen())
            {
                Matrix matrix = new Matrix();
                matrix.Translate(pos.X, pos.Y);
                matrix.Scale(_scale, _scale);
                drawingContext.PushTransform(new MatrixTransform(matrix));

                foreach (DrawObject drawObject in page.GetAllDrawObjects())
                {
                    if (drawObject != null)
                    {
                        drawObject.AddToDrawingContext(drawingContext);
                        drawingContext.Pop();
                    }
                }
            }
            _bitmap.Render(drawingVisual);
            return _bitmap;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
