﻿using System.Windows;
using System.Windows.Controls;
using TouchAndTeachWindows.Comm;

namespace TouchAndTeachWindows.Menu
{
    /// <summary>
    /// Logique d'interaction pour DevicesList.xaml
    /// </summary>
    public partial class DevicesList : UserControl
    {

        private readonly DevicesManager _devicesManager;
        public DevicesList()
        {
            InitializeComponent();
            _devicesManager = DevicesManager.GetDevicesManager();
            DevicesListBox.ItemsSource = _devicesManager.GetDevicesPermissions();
        }

        private void fullAccessButton_Pressed(object sender, RoutedEventArgs e)
        {
            _devicesManager.SetGlobalMode(DevicesManager.Mode.FullAccess);
            FullAccessButton.Selected = true;
            WallOnlyButton.Selected = false;
        }

        private void wallOnlyButton_Pressed(object sender, RoutedEventArgs e)
        {
            _devicesManager.SetGlobalMode(DevicesManager.Mode.Restricted);
            WallOnlyButton.Selected = true;
            FullAccessButton.Selected = false;
        }

        private void DeviceElement_Pressed(object sender, RoutedEventArgs e)
        {
            Device device = (Device) ((DeviceElement) sender).DataContext;
            DevicesManager.Mode mode = _devicesManager.GetMode(device);
            switch (mode)
            {
                case DevicesManager.Mode.FullAccess:
                    _devicesManager.SetMode(device, DevicesManager.Mode.Restricted);
                    break;
                case DevicesManager.Mode.Restricted:
                    _devicesManager.SetMode(device, DevicesManager.Mode.FullAccess);
                    break;
            }
        }
    }
}
