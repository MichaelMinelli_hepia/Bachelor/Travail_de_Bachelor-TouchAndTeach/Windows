﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using TouchAndTeachWindows.Comm;

namespace TouchAndTeachWindows.Menu
{
    /// <summary>
    /// Logique d'interaction pour DeviceElement.xaml
    /// </summary>
    public partial class DeviceElement : UserControl
    {
        #region Events Declaration
        public static readonly RoutedEvent PressedEvent = EventManager.RegisterRoutedEvent("DeviceElementPressed",
            RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(DeviceElement));

        public event RoutedEventHandler Pressed
        {
            add { AddHandler(PressedEvent, value); }
            remove { RemoveHandler(PressedEvent, value); }
        }

        private readonly RoutedEventArgs _pressedArgs = new RoutedEventArgs(PressedEvent);
        #endregion

        public DeviceElement()
        {
            InitializeComponent();
        }

        private void UserControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            RaiseEvent(_pressedArgs);
            e.Handled = true;
        }

        private void UserControl_TouchUp(object sender, TouchEventArgs e)
        {
            RaiseEvent(_pressedArgs);
            e.Handled = true;
        }

        private void UserControl_TouchDown(object sender, TouchEventArgs e)
        {
            e.Handled = true;
        }
    }

    class DeviceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is DevicesManager.Mode)
            {
                return ((DevicesManager.Mode)value) == DevicesManager.Mode.FullAccess ? Brushes.Green : Brushes.Red;
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
