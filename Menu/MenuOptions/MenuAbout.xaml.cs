﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;

namespace TouchAndTeachWindows.Menu.MenuOptions
{
    /// <summary>
    /// Logique d'interaction pour MenuAbout.xaml
    /// </summary>
    public partial class MenuAbout : UserControl
    {
        public MenuAbout()
        {
            InitializeComponent();

            Version version = Assembly.GetExecutingAssembly().GetName().Version;
            NameAndVersion.Text = "nTeach " + version.Major + "." + version.Minor + "." + version.Build;
        }
    }
}
